<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldDataType extends Model
{
    protected $table = 'field_data_type'; 
    protected $primaryKey = 'field_datatype_id';
}
