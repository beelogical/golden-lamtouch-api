<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fields extends Model
{
    protected $table = 'fields';
    protected $primaryKey = 'field_id ';
    public $timestamps = false;
}
