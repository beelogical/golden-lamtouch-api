<?php

namespace App\Http\Controllers;
use Aws\Ses\SesClient;
use Aws\Exception\AwsException;
use JWTAuth;
use App\AppUser;
use App\TemplateData;
use App\Roles;
use App\TemplatePdfs;
use App\Templates;
use App\AppConfig;
use App\OrganizationTypes;
use App\Organization;
use App\Project;
use App\Fields;
use App\FieldDataType;
use App\ProjectFields;
use App\Tokens;
use App\ProjectFieldDropdown;
use App\PrintJobs;
use App\Payment;
use App\Imports\TemplateDataImport;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PHPMailer\PHPMailer\PHPMailer;
use Mail;
use Aws\Common\Aws;
use DB;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Carbon\Carbon;
use Razorpay\Api\Api;
use Session;
use Exception;


class ApiController extends Controller
{
    // public function register(Request $request)
    // {
    //     $input = json_decode(file_get_contents('php://input', true));
       
    //     $data['full_name'] = $input->full_name;
    //     $data['email'] = $input->email;
    //     $data['phone'] = $input->phone;
    //     $data['address'] = $input->address;
    //     $data['profile_image_url'] = $input->profile_image_url;
    //     $data['username'] = $input->username;
    //     $data['role_id'] = $input->role_id;
    //     $data['password'] = $input->password;
    // 	//Validate data
    //     $validator = Validator::make($data, [
    //         'username' => 'required|string',
    //         'email' => 'required|email|unique:app_users',
    //         'password' => 'required|string|min:6|max:50',
            
    //     ]);
    //     //Send failed response if request is not valid
    //     if ($validator->fails()) {
    //         return response()->json(['error' => $validator->messages()], 200);
    //     }

    //     //Request is valid, create new user
    //     $user = AppUser::create([
    //     	'full_name' => $input->full_name,
    //         'email' => $input->email,
    //         'phone' => $input->phone,
    //         'address' => $input->address,
    //         'profile_image_url' => $input->profile_image_url,
    //         'username' => $input->username,
    //     	'role_id' => $input->role_id,
    //     	'password' => bcrypt($input->password),
    //         'updated_at' => date("Y-m-d H:i:s"),
    //         'created_at' => date("Y-m-d H:i:s")
    //     ]);
        
    //     return response()->json([
    //         'status' => "200",
    //         'message' => 'User created successfully',
    //         'data' => $user
    //     ], Response::HTTP_OK);
    // }
 
    public function authenticate(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $credentials['username'] = $input->username;
        $credentials['password'] = $input->password;
        //valid credential
        $validator = Validator::make($credentials, [
            'username' => 'required|string',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            $responseJson = array("status" => "250" , "message" => "username and password is required.");
            return json_encode($responseJson);
        }
        //  $isDeleted = AppUser::where('username',$credentials['username'])->select('is_deleted')->get()->first();
        //  if($isDeleted->is_deleted == 1 ){
        //     $responseJson = array("status" => "250" , "message" => "Invalid user " );
        //     return json_encode($responseJson);
        //  }
       
        //Request is validated
        //Crean token
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'status' => "400",
                	'message' => 'Invalid credentials .',
                ], 400);
            }
        } catch (JWTException $e) {
    	return $credentials;
            return response()->json([
                	'status' => "250",
                	'message' => 'Could not create token.',
                ], 500);
        }
        
            $currentUser = Auth::user();
            $roles = Roles::get()->all();
            $roleArr = [];
            if(!empty($roles)){
                foreach($roles as $role){
                    $roleArr[$role->role_id] = $role->role_title;
                }
            }
            //Token created, return with success response and jwt token
            return response()->json([
                'status' => "200",
                'accessToken' => $token,
                'data' => array('name' => $currentUser->full_name, 'user_id' => $currentUser->app_user_id, 'username' => $currentUser->username, 'roleId' => $currentUser->role_id)
            ]);
    }
 
    public function logout(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $data['token'] = $input->token;
        //valid credential
        $validator = Validator::make($data, [
            'token' => 'required'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            $responseJson = array("status" => "250" , "message" => "token is required.");
            return json_encode($responseJson);
        }


		//Request is validated, do logout        
        try {
            JWTAuth::invalidate($input->token);
            $responseJson = array("status" => "200" , "message" => 'User has been logged out');
            return json_encode($responseJson);
        } catch (JWTException $exception) {
            $responseJson = array("status" => "250" , "message" => 'Sorry, user cannot be logged out');
            return json_encode($responseJson);
        }
    }
 
    public function getUser($appUserId, Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));

        $roles = Roles::get()->all();
        $roleArr = [];
        if(!empty($roles)){
            foreach($roles as $role){
                $roleArr[$role->role_id] = $role->role_title;
            }
        }
        
        $user = AppUser::where('app_user_id', $appUserId)->get()->first();
       

        if($user != null){
            $response = [];
            $response['appUserId'] = $user->app_user_id;
            $response['fullName'] = $user->full_name;
            $response['email'] = $user->email;
            $response['phone'] = $user->phone;
            $response['address'] = $user->address;
            $response['profileImageUrl'] = $user->profile_image_url;
            $response['username'] = $user->username;
            $response['roleId'] = $user->role_id;
            $responseJson = array("status" => "200" , "message" => "Success" , "data" => $response);
            return json_encode($responseJson);
        }else{
            $responseJson = array("status" => "250" , "message" => "Invailid user id" );
            return json_encode($responseJson);
        }
    }
 
    public function addOrganization($organizationId = null, Request $request)
    {
       $input = json_decode(file_get_contents('php://input', true));

       $var['organizationName']  =$input->organizationName;
       $validator = Validator::make( $var, [
           'organizationName' => 'required|string|min:3|max:30',

       ]);

       if ($validator->fails()) {
           $responseJson = array("status" => "250" , "message" => "The organization name field is required.");
           return json_encode($responseJson);
       }

        if($organizationId != 0){
            $organization = Organization::find($organizationId);
            $data['organization_id'] = $organization->organization_id;
            $organization->organization_name = $data['organizationName'] = $input->organizationName;
            $organization->description = $data['description'] = $input->description;
            $organization->organization_email = $data['organizationEmail'] = $input->organizationEmail;
            $organization->organization_type_id  = $data['organizationTypeId '] = $input->organizationTypeId ;
             if($organization->save()){
                 $organization = Organization::where('organization_id', $data['organization_id'] )->get()->first();
                 $response = [];
                 $response['organizationId'] = $organization->organization_id;
                 $response['organizationName'] = $organization->organization_name;
                 $response['description'] = $organization->description;
                 $response['organizationTypeId'] = $organization->organization_type_id;
                 $response['organizationEmail'] = $organization->organization_email;
                 $responseJson = array("status"=>"200" , "message"=>"Organization updated", "data"=>$response);
                 return json_encode($responseJson);
             } else {
                 $responseJson = array("status"=>"250", "message"=>"Updation of Organization failed.");
                 return json_encode($responseJson);
             }
        }else{
            
            $var['organizationName']  =$input->organizationName;
            $validator = Validator::make( $var, [
                'organizationName' => 'required|string|min:3|max:30',
     
            ]);
     
            if ($validator->fails()) {
                $responseJson = array("status" => "250" , "message" => "The organization name field is required.");
                return json_encode($responseJson);
            }
            $data['organizationName'] = $input->organizationName;
            $data['description'] = $input->description;
            $data['organizationTypeId'] = $input->organizationTypeId;
            $data['organizationEmail'] = $input->organizationEmail;
            
            $organizations = new Organization();
            $organizations->organization_name = $data['organizationName'];
            $organizations->description = $data['description'];
            $organizations->organization_type_id = $data['organizationTypeId'];
            $organizations->organization_email = $data['organizationEmail'];

            if($organizations->save()){

                $organization = Organization::where('organization_id', $organizations->organization_id)->get()->first();
                 $response = [];
                 $response['organizationId'] = $organization->organization_id;
                 $response['organizationName'] = $organization->organization_name ;
                 $response['description'] = $organization->description;
                 $response['organizationTypeId'] = $organization->organization_type_id;
                 $response['organizationEmail'] = $organization->organization_email;
                $responseJson = array("status"=>"200" , "message"=>"Organization created successfully", "data"=>$response);
                return json_encode($responseJson);
            } else {
                $responseJson = array("status"=>"250", "message"=>"Failed");
                return json_encode($responseJson);
            }      
        }
    }  

    public function addUser($appUserId = null, Request $request)
    {
       $input = json_decode(file_get_contents('php://input', true));      
        $roles = Roles::get()->all();
        $roleArr = [];
        if(!empty($roles)){
            foreach($roles as $role){
                $roleArr[$role->role_id] = $role->role_title;
            }
        }
        $fullName['fullName'] =$input->fullName;
        $validator = Validator::make($fullName, [
            'fullName' => 'required|string|min:3|max:30|regex:/^[a-zA-Z]+$/u',

        ]);
        if ($validator->fails()) {
            $responseJson = array("status" => "250" , "message" => "The name field is required.");
            return json_encode($responseJson);
        }

        if($appUserId != 0){
            $users = AppUser::find($appUserId);
            $data['app_user_id'] = $users->app_user_id;
            $users->full_name = $data['full_name'] = $input->fullName;
            $users->phone = $data['phone'] = $input->phone;
            $users->address = $data['address'] = $input->address;
            $users->profile_image_url = $data['profile_image_url'] = $input->profileImageUrl;
            $users->username = $data['username'] = $input->username;
             if($users->save()){
                 $user = AppUser::where('app_user_id', $data['app_user_id'])->get()->first();
                 $response = [];
                 $response['appUserId'] = $user->app_user_id;
                 $response['fullname'] = $user->full_name;
                 $response['email'] = $user->email;
                 $response['phone'] = $user->phone;
                 $response['address'] = $user->address;
                 $response['profileImageUrl'] = $user->profile_image_url;
                 $response['username'] = $user->username;
                 $response['roleId'] = $user->role_id;
                 $responseJson = array("status"=>"200" , "message"=>"User updated" , "data"=>$response);
                 return json_encode($responseJson);
             } else {
                 $responseJson = array("status"=>"250", "message"=>"Updation of user failed.");
                 return json_encode($responseJson);
             }
        }else{
            $allUser = AppUser::get()->all();
            $userNameArr = $emailArr = [];
            if(!empty($allUser)){
                foreach($allUser as $user){
                    $emailArr[] = $user->email;
                    $userNameArr[] = $user->username;
                }
            }

            if(in_array($input->email, $emailArr)){
                $responseJson = array("status"=>"250", "message"=>"Duplicate email.");
                return json_encode($responseJson);
            }
            if(in_array($input->username, $userNameArr)){
                $responseJson = array("status"=>"250", "message"=>"Duplicate username.");
                return json_encode($responseJson);
            }
            $fullName['fullName'] =$input->fullName;
            $validator = Validator::make($fullName, [
                'fullName' => 'required|string|min:3|max:30|regex:/^[a-zA-Z]+$/u',

            ]);
    
            //Send failed response if request is not valid
            if ($validator->fails()) {
                $responseJson = array("status" => "250" , "message" => "The name field is required.");
                return json_encode($responseJson);
            }

            $users = new AppUser();
            $users->full_name = $input->fullName ;
            $users->phone = $input->phone;
            $users->address = $input->address;
            $users->profile_image_url = $input->profileImageUrl;
            $users->username = $input->username;
            $users->role_id = $input->roleId;
            $users->email = $input->email;
            $users->password = bcrypt($input->password);
            if($users->save()){

                $user = AppUser::where('app_user_id', $users->app_user_id)->get()->first();
                $response = [];
                $response['appUserId'] = $user->app_user_id;
                $response['fullname'] = $user->full_name;
                $response['email'] = $user->email;
                $response['phone'] = $user->phone;
                $response['address'] = $user->address;
                $response['profileImageUrl'] = $user->profile_image_url;
                $response['username'] = $user->username;
                $response['roleId'] = $user->role_id;

                $subject = 'New User Registration';
                $files = [];
                $emailAndbody = [];
                $emailAndbody[0]['email'] = $user->email;
                $name = preg_replace('/\s+/', '_','');
                $emailAndbody[0]['html'] = 'Your have successfully registered. Please login using below credentials.

                Username : '.$user->username.'
                Password : '.$input->password;
                $status = $this->sendMail($subject, $emailAndbody, $files);
                
                $responseJson = array("status"=>"200" , "message"=>"User created successfully" , "data"=>$response);
                return json_encode($responseJson);
            } else {
                $responseJson = array("status"=>"250", "message"=>"User creation failed.");
                return json_encode($responseJson);
            }      
        }
    }  

    public function register(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));

        $roles = Roles::get()->all();
        $roleArr = [];
        if(!empty($roles)){
            foreach($roles as $role){
                $roleArr[$role->role_id] = $role->role_title;
            }
        }

        $data['full_name'] = $input->fullName;
        $data['email'] = $input->email;
        $data['phone'] = $input->phone;
        $data['address'] = $input->address;
        $data['profile_image_url'] = $input->profileImageUrl;
        $data['username'] = $input->username;
        $data['role_id'] = $input->roleId;
        $data['password'] = $input->password;
    	
        $validator = Validator::make($data, [
            'username' => 'required|string|unique:app_users',
            'email' => 'required|email|unique:app_users',
            'password' => 'required|string|min:6|max:50',
            
        ]);
        if ($validator->fails()) {
            $responseJson = array("status" => "250" , "message" => "The username, email, password fields is required.");
            return json_encode($responseJson);
        }

        $appUsers = new AppUser();
        $appUsers->full_name = $data['full_name'];
        $appUsers->email = $data['email'];
        $appUsers->phone = $data['phone'];
        $appUsers->address = $data['address'];
        $appUsers->profile_image_url = $data['profile_image_url'];
        $appUsers->username = $data['username'];
        $appUsers->role_id = $data['role_id'];
        $appUsers->password = bcrypt($data['password']);
        
        if($appUsers->save()) {
            $appUsersData = AppUser::where('app_user_id', $appUsers->app_user_id)->get()->first();
            $response = [];
            $response['appUserId'] = $appUsersData->app_user_id;
            $response['fullname'] = $appUsersData->full_name;
            $response['email'] = $appUsersData->email;
            $response['phone'] = $appUsersData->phone;
            $response['address'] = $appUsersData->address;
            $response['profileImageUrl'] = $appUsersData->profile_image_url;
            $response['username'] = $appUsersData->username;
            $response['roleId'] = $appUsersData->role_id;

            $responseJson = array("status"=>"200", "message"=>"User created successfully",'data' =>$response);
            return json_encode($responseJson);        
        } else {
            $responseJson = array("status"=>"250", "message"=>" User creation failed");
            return json_encode($responseJson);    
        }  
    }

    public function getOrganizationTypes(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $organizationArr = $final=[];
        $organization = OrganizationTypes::get()->all();
           foreach($organization as  $types){
               $organizationArr=[];
               $organizationArr['organizationTypeId']=$types->organization_type_id;
               $organizationArr['organizationTypeName']=$types->organization_type_name;
               $final[]= $organizationArr;
           }   
        if($organization !=null) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $final );
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function getRoles(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $rolesArr = $final=[];
        $roles = Roles::get()->all();
        foreach($roles as  $types){
            $rolesArr=[];
            $rolesArr['roleId']=$types->role_id;
            $rolesArr['roleTitle']=$types->role_title;
            $final[]= $rolesArr;
        }
        if($roles !=null) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $final );
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }

    public function resetPassword(Request $request) 
    {
        $input = json_decode(file_get_contents('php://input', true));
 
        $data['user_id'] = $input->userId;
        $data['new_password'] = $input->newPassword;

        $validator = Validator::make($data, [
           'new_password' =>'required|string|min:6|max:50'
            ]);

            if ($validator->fails()) {
                $responseJson = array("status" => "250" , "message" => "new password is required.");
                return json_encode($responseJson);
            }
    

        $appUser = AppUser::where('app_user_id', $data['user_id'])->get()->first();
        if($appUser != null) {
            $appUser->password = bcrypt($data['new_password']);
            if($appUser->save()) {
                $responseJson = array("status"=>"200", "message"=>"Password change successfully ");
                return json_encode($responseJson);        
            } else {
                $responseJson = array("status"=>"250", "message"=>"Password change failed.");
                return json_encode($responseJson);    
            }  
        }
        else {
            $responseJson = array("status"=>"250", "message"=>"Invalid user id");
            return json_encode($responseJson);
        }
    }      
    
    public function changePassword(Request $request) 
    {
        $input = json_decode(file_get_contents('php://input', true));

        $data['user_id'] = $input->userId;
        $data['old_password'] = $input->oldPassword;
        $data['new_password'] = $input->newPassword;

        $validator = Validator::make($data, [
            //'old_password'     => 'required|string|min:6|max:50', 
            'new_password'     => 'required|string|min:6|max:50',  
        ]);
       
        if ($validator->fails()) {
            $responseJson = array("status" => "250" , "message" => "old password and new password is required.");
            return json_encode($responseJson);
        }

         $user = AppUser::find(auth()->user()->app_user_id);
         $appUser = AppUser::where('app_user_id', $input->userId)->get()->first();
         if($appUser != null) {
            if(!Hash::check($data['old_password'], $user->password)){
               // return response()->json(['error' => 'Incorrect Current password.']);
                $responseJson = array("status" => "250" , "message" => "Incorrect Current password.");
                return json_encode($responseJson);
            }else{
                $user->password =bcrypt($data['new_password']) ;
            
            }
            if($user->save()) {
                $responseJson = array("status"=>"200", "message"=>"Password change successfully ");
                return json_encode($responseJson);        
            } else {
                $responseJson = array("status"=>"250", "message"=>"Password change failed.");
                return json_encode($responseJson);    
            }
        }
        else {
            $responseJson = array("status"=>"250", "message"=>"Invalid user id");
            return json_encode($responseJson);
        }
    }      

    public function getOrganizations(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
       
        $organizationArr = $response =[];
        $organization = Organization::get()->all();
      
           foreach($organization as $key=> $types){
               $organizationArr=[];
               $organizationArr['organizationId'] = $types->organization_id;
               $organizationArr['organizationName'] = $types->organization_name;
               $organizationArr['description'] = $types->description;
               $organizationArr['organizationTypeId'] = $types->organization_type_id;
               $organizationArr['organizationEmail'] = $types->organization_email;
               $response[]= $organizationArr;
           }
         
        if($organization !=null) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $response );
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function getProjects($organization_id,Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $ProjectArr = $response =[];
        $Project = Project::where('organization_id',$organization_id)->get()->all(); 
        if(count($Project) > 0){ 
            foreach($Project as $key => $types){
                $ProjectFieldArr = $ProjectArr = [];
                $usedTokens = Tokens::where('project_id', $types->project_id)->where('isActive', 0)->get()->all(); 
                $ProjectArr['projectId'] = $types->project_id;
                $ProjectArr['projectName'] = $types->project_name;
                $ProjectArr['projectEmail'] = $types->project_email;
                $ProjectArr['description'] = $types->description;
                $ProjectArr['noOfTokens'] = $types->no_of_tokens;
                $ProjectArr['noOfUsedTokens'] = count($usedTokens);
                $ProjectArr['organizationId'] = $types->organization_id;
                $ProjectArr['paymentType'] = $types->payment_type;
                $ProjectArr['amount'] = $types->amount;
                $ProjectArr['withGST'] = $types->with_or_without_gst;
                $field = ProjectFields::where('project_id', $types->project_id)->get()->all(); 
                if(count($field) > 0){ 
                    foreach($field as $key => $typ){
                        $pro = [];
                        $pro['projectFieldId'] = $typ->project_field_id;
                        $pro['projectFieldName'] = $typ->project_field_name;
                        $pro['projectFieldType'] = $typ->project_field_type;
                        $pro['projectFieldMaxLength'] = $typ->max_length;
                        $dropdownArr =  [];
                        $projectFieldDropdown = ProjectFieldDropdown::where('project_field_id',$typ->project_field_id)->get()->all();
                        $dropdownArr = [];
                        if(count($projectFieldDropdown) > 0){
                            foreach($projectFieldDropdown as $projectFieldDropdown){
                                $dropdownArr[] = $projectFieldDropdown->name;
                            }
                        }
                        $pro['options'] = $dropdownArr; 
                        $ProjectFieldArr[] = $pro; 
                    }  
                }
                $ProjectArr['projectFields'] = $ProjectFieldArr;
                $response[] = $ProjectArr;  
            }
        }
        if($Project != null) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $response);
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }

    public function getProjectById($projectId,Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $orgArr = $ProjectArr = $response =[];
        $organization = Organization::get()->all();
        if(count($organization) > 0){
            foreach($organization as $org){
                $orgArr[$org->organization_id] = $org->organization_name;
            }
        }
        $Project = Project::where('project_id', $projectId)->get()->first(); 
        if($Project != null){ 
            $ProjectFieldArr = $ProjectArr = [];
            $usedTokens = Tokens::where('project_id', $projectId)->where('isActive', 0)->get()->all(); 
            $ProjectArr['projectId'] = $Project->project_id;
            $ProjectArr['organizationName'] = $orgArr[$Project->organization_id];
            $ProjectArr['projectName'] = $Project->project_name;
            $ProjectArr['projectEmail'] = $Project->project_email;
            $ProjectArr['description'] = $Project->description;
            $ProjectArr['noOfTokens'] = $Project->no_of_tokens;
            $ProjectArr['noOfUsedTokens'] = count($usedTokens);
            $ProjectArr['organizationId'] = $Project->organization_id;
            $ProjectArr['paymentType'] = $Project->payment_type;
            $ProjectArr['amount'] = $Project->amount;
            $ProjectArr['withGST'] = $Project->with_or_without_gst;
            $field = ProjectFields::where('project_id', $Project->project_id)->get()->all(); 
            if(count($field) > 0){ 
                foreach($field as $key => $typ){
                    $pro = [];
                    $pro['projectFieldId'] = $typ->project_field_id;
                    $pro['projectFieldName'] = $typ->project_field_name;
                    $pro['projectFieldType'] = $typ->project_field_type;
                    $pro['projectFieldMaxLength'] = $typ->max_length;
                    $dropdownArr =  [];
                    $projectFieldDropdown = ProjectFieldDropdown::where('project_field_id',$typ->project_field_id)->get()->all();
                    $dropdownArr = [];
                    if(count($projectFieldDropdown) > 0){
                        foreach($projectFieldDropdown as $projectFieldDropdown){
                            $dropdownArr[] = $projectFieldDropdown->name;
                        }
                    }
                    $pro['options'] = $dropdownArr; 
                    $ProjectFieldArr[] = $pro; 
                }  
            }
            $ProjectArr['projectFields'] = $ProjectFieldArr;
            //$response[] = $ProjectArr;  
        }
        if($Project != null) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $ProjectArr);
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }

    public function forgetPassword(Request $request){
        $input = json_decode(file_get_contents('php://input', true));
        $email = $input->email;
        $userData = AppUser::where('email', $input->email)->get()->first();
        if($userData != null){
            $subject = 'Forget Password';
            $files = [];
            $emailAndbody = [];
            $emailAndbody[0]['email'] = $email;
            $name = preg_replace('/\s+/', '_', $userData->full_name);
            $emailAndbody[0]['html'] = 'Please click on link to reset password 
            http://'.$_SERVER['HTTP_HOST'].'/golden-lamtouch/?newpassword&userId='.$userData->app_user_id.'&username='.strtolower($name);
            $status = $this->sendMail($subject, $emailAndbody, $files);
            if($status){
                $responseJson = array("status"=>"200", "message"=>"Reset Password link sent successfully.");
                return json_encode($responseJson);   
            }else{
                $responseJson = array("status"=>"250", "message"=>"Sending mail failed");
                return json_encode($responseJson);
            }
        }else{
            $responseJson = array("status"=>"250", "message"=>"Invailid email ");
            return json_encode($responseJson); 
        }
    }

    private function sendMail($subject, $emailAndbody, $files){
        $client = new SesClient([
            'region'  => 'ap-south-1',
            'version' => 'latest',
            'credentials' => [
                    'key' => "AKIAUU7LBDIH3DUPXBRR",
                    'secret' => "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",
            ]
        ]);

        $sender = 'confirmation@goldenicard.com';           
        $sendername = 'Golden Lamtouch';
        $region = 'ap-south-1';
        $subject = $subject;
        $textbody = '';
        $configset = '';   
        // Create a new PHPMailer object.
        $mail = new \PHPMailer\PHPMailer\PHPMailer;
        // Add components to the email.
        $mail->setFrom($sender, $sendername);

        if(count($emailAndbody) > 0){
            foreach($emailAndbody as $emailAndhtml){
                $htmlbody =  $emailAndhtml['html'];
                $mail->addAddress($emailAndhtml['email']);
            }
        }
        
        $mail->IsHTML(true);
        $mail->Subject = $subject;
        $mail->MsgHTML($htmlbody);
        // if(!empty($files)){
        //     foreach($files as $file){
        //         $attachmentUrl =  "EmailAttachments/".$id."/".$file;
        //         $path =  Storage::disk('s3')->url($attachmentUrl);
        //         $mail->addStringAttachment(file_get_contents($path), $file);
        //     }
        // }
        $mail->preSend();
        $message = $mail->getSentMIMEMessage();
        
        try {
            $result = $client->sendRawEmail([
                'RawMessage' => [
                    'Data' => $message
                ]
            ]);
            // If the message was sent, show the message ID.
            $messageId = $result->get('MessageId');
            $response = json_encode(array("status" => "200", "message" => "Email sent."));
            return true;
        } catch (\Throwable $th) {
            $response = json_encode(array("status" => "250", "message" => $th->getMessage()));
            return false;
        }
    }
    public function getAllUsers(Request $request)
    {
    $input = json_decode(file_get_contents('php://input', true));
    $appuserArr = $response =[];
    $appuser = AppUser::get()->all();
    $roles = Roles::get()->all();
    $roleArr = [];
    if(!empty($roles)){
        foreach($roles as $role){
            $roleArr[$role->role_id] = $role->role_title;
        }
    }
       foreach($appuser as $key=> $types){
          $appuserArr=[];
          $appuserArr['id'] = $types->app_user_id;
          $appuserArr['fullName'] = $types->full_name;
          $appuserArr['email'] = $types->email;
          $appuserArr['phone'] = $types->phone;
          $appuserArr['address'] = $types->address;
          $appuserArr['profileImageUrl'] = $types->profile_image_url;
          $appuserArr['username'] = $types->username;
          $appuserArr['roleId'] = $types->role_id;
          $response[] = $appuserArr;
       }
     
        if($appuser !=null) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $response );
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function deleteUser($appUserId  , Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $id = AppUser::find($appUserId);
        if($id != null) { 
            if($id->delete()) {
                 $responseJson = array("status"=>"200", "message"=>"User deleted successfully", "data"=> $id);
                 return json_encode($responseJson);              
            }else{
                $responseJson = array("status"=>"250", "message"=>"Failed ", "data"=>[]);
                return json_encode($responseJson);              
            }
        }else {
            $responseJson = array("status"=>"250", "message"=>"Invalid user id ", "data"=>[]);
            return json_encode($responseJson); 

        }
    }

    public function deleteProject($projectId  , Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        try{
            DB::beginTransaction();
            $id = Project::find($projectId);
            if($id != null) { 
                PrintJobs::where('project_id', $projectId)->delete();
                $prjFields = ProjectFields::where('project_id', $projectId)->get()->all();
                $prjFieldIds = [];
                if(count($prjFields) > 0){
                    foreach($prjFields as $prjField){
                        $prjFieldIds[] = $prjField->project_field_id;
                    }
                }
                ProjectFieldDropdown::whereIn('project_field_id', $prjFieldIds)->delete();
                ProjectFields::where('project_id', $projectId)->delete();
                Templates::where('project_id', $projectId)->delete();
                $tempDataIds = [];
                $tempData = TemplateData::where('project_id', $projectId)->get()->all();
                if(count($tempData) > 0){
                    foreach($tempData as $tempData){
                        $tempDataIds[] = $tempData->template_data_id;
                    }
                    TemplatePdfs::whereIn('template_data_id', $tempDataIds)->delete();
                    TemplateData::where('project_id', $projectId)->delete();
                }
                Tokens::where('project_id', $projectId)->delete();
                if($id->delete()) {
                    DB::commit();
                    $responseJson = array("status"=>"200", "message"=>"Project deleted successfully", "data"=> $id);
                    return json_encode($responseJson);              
                }else{
                    DB::rollback();
                    $responseJson = array("status"=>"250", "message"=>"Failed ", "data"=>[]);
                    return json_encode($responseJson);              
                }
            }else {
                DB::rollback();
                $responseJson = array("status"=>"250", "message"=>"Invalid project id ", "data"=>[]);
                return json_encode($responseJson); 

            }
            
        }catch(\Exception $e){
            DB::rollback();
        }
    }
      
    public function deleteOrganization($organizationId ,Request $request)
    {
       $input = json_decode(file_get_contents('php://input', true));
        try{
            DB::beginTransaction();
            $id = Organization::find($organizationId); 
            if($id != null) { 
                $projectIds = [];
                $orgProjects = Project::where('organization_id', $organizationId)->get()->all();
                if(count($orgProjects) > 0){
                    foreach($orgProjects as $orgProject){
                        $projectIds[] = $orgProject->project_id;
                    }
                }
                
                if(count($projectIds) > 0){
                    $printJobs = PrintJobs::whereIn('project_id', $projectIds)->get()->all();
                    if(count($printJobs) > 0){
                        PrintJobs::whereIn('project_id', $projectIds)->delete();
                    }
                   
                    $prjFields = ProjectFields::whereIn('project_id', $projectIds)->get()->all();
                    $prjFieldIds = [];
                    if(count($prjFields) > 0){
                        foreach($prjFields as $prjField){
                            $prjFieldIds[] = $prjField->project_field_id;
                        }
                    }
                    
                    $projectFieldDropdown = ProjectFieldDropdown::whereIn('project_field_id', $prjFieldIds)->get()->all();
                    if(count($projectFieldDropdown) > 0){
                        ProjectFieldDropdown::whereIn('project_field_id', $prjFieldIds)->delete();
                    }
                    $projectFields = ProjectFields::whereIn('project_id', $projectIds)->get()->all();
                    if(count($projectFields) > 0){
                        ProjectFields::whereIn('project_id', $projectIds)->delete();
                    }
                    $templates = Templates::whereIn('project_id', $projectIds)->get()->all();
                    if(count($templates) > 0){
                        Templates::whereIn('project_id', $projectIds)->delete();
                    }
                    
                    $tempDataIds = [];
                    $tempData = TemplateData::whereIn('project_id', $projectIds)->get()->all();
                    if(count($tempData) > 0){
                        foreach($tempData as $tempData){
                            $tempDataIds[] = $tempData->template_data_id;
                        }

                        $templatePdfs = TemplatePdfs::whereIn('template_data_id', $tempDataIds)->get()->all();
                        if(count($templatePdfs) > 0){
                            TemplatePdfs::whereIn('template_data_id', $tempDataIds)->delete();
                        }
                        $templateData = TemplateData::whereIn('project_id', $projectIds)->get()->all();
                        if(count($templateData) > 0){
                            TemplateData::whereIn('project_id', $projectIds)->delete();
                        }
                    }    
                    $tokens = Tokens::whereIn('project_id', $projectIds)->get()->all();
                    if(count($tokens) > 0){
                        Tokens::whereIn('project_id', $projectIds)->delete();
                    }
                    Project::whereIn('project_id', $projectIds)->delete();
                }
                //var_dump($projectIds);exit;
                if($id->delete()) {
                    DB::commit();
                    $responseJson = array("status"=>"200", "message"=>"Organization deleted successfully", "data"=> $id);
                    return json_encode($responseJson);              
                }else{
                    DB::rollback();
                    $responseJson = array("status"=>"250", "message"=>"Failed ", "data"=>[]);
                    return json_encode($responseJson);              
                }
            }else {
                DB::rollback();
                $responseJson = array("status"=>"250", "message"=>"Invalid organization id ", "data"=>[]);
                return json_encode($responseJson); 
            }
        }catch(\Exception $e){
            DB::rollback();
        }
        // $id = Organization::find($organizationId);    
        // if($id != null) {
        //     $id->is_deleted = 1;
        //     if($id->save()){
        //         $responseJson = array("status"=>"200", "message"=>"Organization deleted successfully" );
        //         return json_encode($responseJson);              
        //     } else {
        //         $responseJson = array("status"=>"250", "message"=>"Failed");
        //         return json_encode($responseJson);              
        //     }
        // }else {
        //     $responseJson = array("status"=>"250", "message"=>"Invalid organization id ");
        //     return json_encode($responseJson); 

        // }
    }
    public function getFields(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $fieldsArr = $response =[];
        $fields = Fields::get()->all();
    
        foreach($fields as $key=> $types){
            $fieldsArr = [];
            $fieldsArr['id'] = $types->field_id;
            $fieldsArr['name'] = $types->field_name;
            $response[] = $fieldsArr;
        }
        
        if($fields != null) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $response );
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function getFieldDataType(Request $request)
    {
       
        $input = json_decode(file_get_contents('php://input', true));
    
        $fieldsArr = $response = [];
        $fields = FieldDataType::get()->all();
    
        foreach($fields as $key=> $types){
            $fieldsArr = [];
            $fieldsArr['id'] = $types->field_datatype_id ;
            $fieldsArr['name'] = $types->field_datatype_name;
            $response[] = $fieldsArr;
        }
        
        if($fields != null) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $response);
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function addProject($projectId, Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        if($projectId != 0){
            $data['projectName'] = $input->projectName;
            $data['description'] = $input->description;
            $data['noOfTokens'] = $input->noOfTokens;
            $data['organizationId'] = $input->organizationId;
            $duplicateProjectName = Project::where('organization_id', $input->organizationId)->where('project_name',$input->projectName)->count();
            $duplicateProjectName1 = Project::where('organization_id', $input->organizationId)->where('project_name',$input->projectName)->get()->first();
            if(($duplicateProjectName == 1 && ($duplicateProjectName1->project_id == $projectId)) || $duplicateProjectName == 0){
                $data['projectEmail'] = $input->projectEmail;
                $data['paymentType'] = $input->paymentType;
                $data['withGST'] = $input->withGST;
                $data['amount'] = $input->amount;
                $projectData = Project::where('project_id',$projectId)->get()->first();
                $noOfMoreTokens = 0;
                $project = Project::find($projectId);
                $noOfExistingTokens = $projectData->no_of_tokens;
                if($data['noOfTokens'] >  $projectData->no_of_tokens)
                {
                    $noOfMoreTokens = $data['noOfTokens'] - $projectData->no_of_tokens;
                    $project->no_of_tokens = $data['noOfTokens'];
                }
                
                $project->project_name = $data['projectName'];
                $project->description = $data['description'];
                $project->organization_id = $data['organizationId'];
                $project->project_email = $data['projectEmail'];
                $project->payment_type = $data['paymentType'];
                $project->amount = $data['amount'];
                $project->with_or_without_gst = $data['withGST'];
                if($project->save()){
                    $projectId = $project->project_id;
                    $response = [];
                    $response['projectId'] = $projectId;
                    $response['projectName'] = $data['projectName'];
                    $response['description'] = $data['description'];
                    $response['noOfTokens'] = $data['noOfTokens'];
                    $response['organizationId'] = $data['organizationId'];
                    $response['projectEmail'] = $data['projectEmail'];
                    $response['paymentType'] = $data['paymentType'];
                    $response['withGST'] =  $data['withGST'];
                    $response['amount'] =  $data['amount'];
                    $projectFields = ProjectFields::where('project_id', $projectId)->get()->all();
                    if(count($projectFields) > 0){
                        $projectFieldIds = [];
                        foreach($projectFields as $projectField){
                            $projectFieldIds[] = $projectField->project_field_id;
                        }
                        ProjectFieldDropdown::whereIn('project_field_id', $projectFieldIds)->delete();
                        ProjectFields::where('project_id', $projectId)->delete();
                    }
                        
                    $fieldsData = [];
                    if(count($input->projectFields) > 0){
                        foreach($input->projectFields as $fields){
                            //$fieldsData[] = array('project_id' => $projectId, 'project_field_name' => $fields->projectFieldName, 'project_field_type' => $fields->projectFieldType);
                            
                            $fieldName = $fields->projectFieldName;
                            $fieldNameData = Fields::where('field_name', str_replace(' ', '_', str_replace('.', '', $fieldName)))->get()->first(); 
                            if($fieldNameData == null){
                                $field = new Fields();
                                $field->field_name = str_replace(' ', '_', str_replace('.', '', $fieldName));
                                $field->save();
                            }

                            $fieldType =  $fields->projectFieldType;
                            $fieldTypeData = FieldDataType::where('field_datatype_name', $fieldType)->get()->first();  
                            if($fieldTypeData == null){
                                $field = new FieldDataType();
                                $field->field_datatype_name = $fieldType;
                                $field->save();
                            }
                            $projectFields = new ProjectFields();
                            $projectFields->project_id = $projectId;
                            $projectFields->project_field_name = str_replace(' ', '_', str_replace('.', '', $fields->projectFieldName));
                            $projectFields->project_field_type = $fields->projectFieldType;
                            $projectFields->max_length = $fields->projectFieldMaxLength;
                            $projectFields->save();
                            if($fields->projectFieldType == 'dropdown'){
                                if(count($fields->options) > 0){
                                    $dropdownData = [];
                                    foreach($fields->options as $options){
                                        $dropdownData[] = array('dropdown_id' => null, 'name' => $options, 'project_field_id' => $projectFields->project_field_id);
                                    }
                                    if(count($dropdownData) > 0){
                                        ProjectFieldDropdown::insert($dropdownData);
                                    }
                                }
                            }
                        }
                    }
                    $tokenData = [];
                    
                    if($noOfMoreTokens > 0){
                        for ($x = 1; $x <= $noOfMoreTokens; $x++) {
                            $serialNo = $noOfExistingTokens+$x;
                            if($serialNo <= 9){
                                $serialNo = '0'.$serialNo;
                            }
                            $serialNo = strftime("%y").'00'.$serialNo;
                            $var = $this->getToken($projectId);
                            $tokenData[] = array('token' => $var, 'isActive' => '1', 'project_id' => $projectId, 'created_at' => date("Y-m-d H:i:s"), 'serial_no' => $serialNo);
                        }

                        if(count($tokenData) > 0){
                            if(Tokens::insert($tokenData)) {
                                $responseJson = array("status"=>"200", "message"=>"Project updated.", "data"=>$response);
                                return json_encode($responseJson);   
                            }else{
                                $responseJson = array("status"=>"250", "message"=>"Token updation failed", "data"=>[]);
                                return json_encode($responseJson);  
                            }
                        }
                    }else{
                        $responseJson = array("status"=>"200", "message"=>"Project updated successfully.", "data"=>$response);
                        return json_encode($responseJson); 
                    }
                        
                }else{
                    $responseJson = array("status"=>"250", "message"=>"Project data updation failed", "data"=>[]);
                    return json_encode($responseJson);  
                } 
            }    
            else{
                $responseJson = array("status"=>"250", "message"=>"Duplicate Project Name", "data"=>[]);
                return json_encode($responseJson);  
            }      
        }
        else{
        $data['projectName'] = $input->projectName;
        $data['description'] = $input->description;
        $data['noOfTokens'] = $input->noOfTokens;
        $data['organizationId'] = $input->organizationId;
        $duplicateName= Project::where('organization_id',$input->organizationId)->where('project_name',$input->projectName)->count();
        if($duplicateName == 0){
            $data['projectEmail'] = $input->projectEmail;
            $data['paymentType'] = $input->paymentType;
            $data['withGST'] = $input->withGST;
            $data['amount'] = $input->amount;
            $project = new Project();
            $project->project_name = $data['projectName'];
            $project->description = $data['description'];
            $project->no_of_tokens = $data['noOfTokens'] ;
            $project->organization_id = $data['organizationId'];
            $project->project_email = $data['projectEmail'];
            $project->payment_type = $data['paymentType'];
            $project->with_or_without_gst = $data['withGST'];
            $project->amount = $data['amount'];
            if($project->save()) {
                $projectId =  $project->project_id;
                $response = [];
                $response['projectId'] =  $projectId;
                $response['projectName'] =  $data['projectName'];
                $response['description'] =  $data['description'];
                $response['noOfTokens'] =$data['noOfTokens'];
                $response['organizationId'] =  $data['organizationId'];
                $response['projectEmail'] =  $data['projectEmail'];
                $response['paymentType'] =  $data['paymentType'];
                $response['withGST'] =  $data['withGST'];
                $response['amount'] =  $data['amount'];
                $projectId =  $project->project_id;
                $fieldsData = [];
                    if(count($input->projectFields) > 0){
                        foreach($input->projectFields as $fields){
                            $fieldsData[] = array('project_id' => $projectId, 'project_field_name' => $fields->projectFieldName, 'project_field_type' => $fields->projectFieldType);
                            $fieldName = $fields->projectFieldName;
                            $fieldNameData = Fields::where('field_name', str_replace(' ', '_', str_replace('.', '', $fieldName)))->get()->first(); 
                            if($fieldNameData == null){
                                $field = new Fields();
                                $field->field_name = str_replace(' ', '_', str_replace('.', '', $fieldName));
                                $field->save();
                            }

                            $fieldType =  $fields->projectFieldType;
                            $fieldTypeData = FieldDataType::where('field_datatype_name', $fieldType)->get()->first();  
                            if($fieldTypeData == null){
                                $field = new  FieldDataType();
                                $field->field_datatype_name = $fieldType;
                                $field->save();
                            }

                            $projectFields = new ProjectFields();
                            $projectFields->project_id = $projectId;
                            $projectFields->project_field_name = str_replace(' ', '_', str_replace('.', '', $fields->projectFieldName));
                            $projectFields->project_field_type = $fields->projectFieldType;
                            $projectFields->max_length = $fields->projectFieldMaxLength;
                            $projectFields->save();
                            if($fields->projectFieldType == 'dropdown'){
                                if(count($fields->options) > 0){
                                    $dropdownData = [];
                                    foreach($fields->options as $options){
                                        $dropdownData[] = array('dropdown_id' => null, 'name' => $options, 'project_field_id' => $projectFields->project_field_id);
                                    }
                                    if(count($dropdownData) > 0){
                                        ProjectFieldDropdown::insert($dropdownData);
                                    }
                                }
                            }
                        }               
                    }
                
                    $tokenData = []; 
                    // if(count($fieldsData) > 0){
                    //     if(ProjectFields::insert($fieldsData)) {
                            for ($x = 1; $x <= $data['noOfTokens']; $x++) {
                                $var = $this->getToken($projectId);
                                $serialNo = $x;
                                if($serialNo <= 9){
                                    $serialNo = '0'.$serialNo;
                                }
                                $serialNo = strftime("%y").'00'.$serialNo;
                                $tokenData[] = array('token' => $var, 'isActive' => '1', 'project_id' => $projectId, 'created_at' => date("Y-m-d H:i:s"), 'serial_no' => $serialNo);
                            }

                            if(count($tokenData) > 0){
                                if(Tokens::insert($tokenData)) {
                                    $responseJson = array("status"=>"200", "message"=>"Project created  successfully.", "data"=>$response);
                                    return json_encode($responseJson);   
                                }else{
                                    $responseJson = array("status"=>"250", "message"=>"Token addition failed", "data"=>[]);
                                    return json_encode($responseJson);  
                                }
                            }
                    //     }else{
                    //         $responseJson = array("status"=>"250", "message"=>"Project field data addition failed", "data"=>[]);
                    //         return json_encode($responseJson);  
                    //     }
                    // }
                }else{
                    $responseJson = array("status"=>"250", "message"=>"Project data addition failed", "data"=>[]);
                    return json_encode($responseJson);  
                }
            }else{
                $responseJson = array("status"=>"250", "message"=>"Duplicate Project Name", "data"=>[]);
                return json_encode($responseJson);  
            }      
        }
    }

    public function getToken($projectId){
        $token =  rand(100000,999999).$projectId;
        $existingTokens = Tokens::where('project_id', $projectId)->get()->all();
        $existingTokensArr = [];
        if(count($existingTokens) > 0){
            foreach($existingTokens as $existingToken){
                $existingTokensArr[] = $existingToken->token;
            }
        }

        if (in_array($token, $existingTokensArr)){
            return $this->getToken($projectId);
        }
        return $token;
    }
    public function getProjectTokens($projectId, Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $tokenArr = [];
        $tokens = Tokens::where('project_id', $projectId)->get()->all(); 
        if(count($tokens) > 0){ 
            foreach($tokens as $key => $token){
                $tokenArr[] = $token->token; 
            }
        }
        if(count($tokens) > 0) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $tokenArr);
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function getProjectTokenData($projectId, Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $tokenArr = [];
        $tokens = Tokens::where('project_id', $projectId)->get()->all(); 
        if(count($tokens) > 0){ 
            foreach($tokens as $key => $token){
                $tokenss['name'] = '';
                $templateData = TemplateData::where('token_id', $token->token_id)->get()->first(); 
                if($templateData != null){
                    if(!empty($templateData->data)){
                        $decodedData = json_decode($templateData->data);
                        if($token->isActive == 0){
                            $tokenss['name'] = isset($decodedData->Name) ? $decodedData->Name : '';
                        }
                    }
                }
                $tokenss['token'] = $token->token;
                $tokenss['tokenSerialNo'] = $token->serial_no; 
                $tokenss['tokenPrintNo'] = $token->print_no; 
                $tokenArr[] = $tokenss;
            }
        }
        if(count($tokens) > 0) {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=> $tokenArr);
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Success", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function getProjectFields($projectId,Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $fieldsArr = [];
        $fields =ProjectFields::where('project_id', $projectId)->get()->all(); 
    
        foreach($fields as $key=> $types){
            $fieldsArr=[];
            $fieldsArr['projectFieldId']=$types->project_field_id ;
            $fieldsArr['projectFieldName']=$types->project_field_name;
            $fieldsArr['projectFieldType']=$types->project_field_type;
            $fieldsArr['projectFieldMaxLength']=$types->max_length;
            $fieldsArr['projectId']=$types->project_id ;

            $projectFieldDropdown = ProjectFieldDropdown::where('project_field_id',$types->project_field_id)->get()->all();
            $dropdownArr = [];
            if(count($projectFieldDropdown) > 0){
                foreach($projectFieldDropdown as $projectFieldDropdown){
                    $dropdownArr[] = $projectFieldDropdown->name;
                }
            }
            $fieldsArr['options'] = $dropdownArr;  

            $response[]= $fieldsArr;
        }
        
        if($fields !=null) {
            $responseJson = array("status"=>"200", "message"=>"Successful", "data"=> $response );
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"Successful", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function getAllProjects(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $ProjectArr = $response =[];
        $Project = DB::table('projects')
       ->join('organizations', 'projects.organization_id', '=', 'organizations.organization_id')
        ->select('projects.project_id','projects.project_name','projects.description','projects.no_of_tokens',
             'projects.organization_id', 'organizations.organization_name','projects.payment_type','projects.amount','projects.with_or_without_gst')
              ->get()->all();

        
            foreach($Project as $key => $types){
                $ProjectFieldArr = $ProjectArr = [];

                $tokenCount = Tokens::where('project_id', $types->project_id)->where('isActive', 0)->count();

                $ProjectArr['projectId'] = $types->project_id;
                $ProjectArr['projectName'] = $types->project_name;
                $ProjectArr['description'] = $types->description;
                $ProjectArr['noOfTokens'] = $types->no_of_tokens;
                $ProjectArr['noOfUsedTokens'] = $tokenCount;
                $ProjectArr['organizationId'] = $types->organization_id;
                $ProjectArr['organizationName'] = $types->organization_name;
                $ProjectArr['paymentType'] = $types->payment_type;
                $ProjectArr['amount'] = $types->amount;
                $ProjectArr['withGST'] = $types->with_or_without_gst;
                $field = ProjectFields::where('project_id', $types->project_id)->get()->all(); 
                if(count($field) > 0){ 
                    foreach($field as $key => $types){
                        $pro = [];
                        $pro['projectFieldId'] = $types->project_field_id;
                        $pro['projectFieldName'] = $types->project_field_name;
                        $pro['projectFieldType'] = $types->project_field_type;
                        $pro['projectFieldMaxLength'] = $types->max_length;
                        $ProjectFieldArr[] = $pro; 
                    }  
                }
                $ProjectArr['projectFields'] = $ProjectFieldArr;
                $response[] = $ProjectArr;  
            }
    
        if($Project != null) {
            $responseJson = array("status"=>"200", "message"=>"successful", "data"=> $response );
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"successful", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function addTemplates($templateId, Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));    
        if($templateId != 0){
            $templates = Templates::where('template_id', $templateId )->get()->first();
            if($templates!=null){
            $data['templateId'] = $templates->template_id;
            $templates->template_name = $data['templateName'] = $input->templateName;
            $templates->print_preference = $data['printPreference'] = $input->printPreference;
            $templates->template_html_front	= $data['templateHtmlFront'] = $input->templateHtmlFront;
            $templates->template_html_back = $data['templateHtmlBack'] = $input->templateHtmlBack;
            $templates->project_id = $data['projectId'] = $input->projectId;
            $templates->template_layout = $data['templateLayout'] = $input->templateLayout;
             if($templates->save()){
                 if(count($input->projectFields) > 0){
                    foreach($input->projectFields as $fields){
                        if(isset($fields->projectFieldId)){
                            $projectFields = ProjectFields::where('project_field_id', $fields->projectFieldId)->get()->first();
                            //$projectFields->project_id = $fields->projectId;
                            $projectFields->project_field_name = $fields->projectFieldName;
                            $projectFields->project_field_type = $fields->projectFieldType;
                            $projectFields->max_length = $fields->projectFieldMaxLength;
                            $projectFields->save();
                        }
                    }
                 }
                 $templates = Templates::where('template_id', $data['templateId'] )->get()->first();
                 $response = [];
                 $response['templateId'] = $templates->template_id;
                 $response['templateName'] = $templates->template_name;
                 $response['printPreference'] = $templates->print_preference;
                 $response['templateHtmlFront'] = $templates->template_html_front;
                 $response['templateHtmlBack'] = $templates->template_html_back;
                 $response['projectId'] = $templates->project_id;
                 $response['templateLayout'] = $templates->template_layout;
                 $responseJson = array("status"=>"200" , "message"=>"Tempaltes updated", "data"=>$response);
                 return json_encode($responseJson);
             } else {
                 $responseJson = array("status"=>"250", "message"=>"Updation of Tempaltes failed.");
                 return json_encode($responseJson);
             }
            }else
             { $responseJson = array("status"=>"250", "message"=>"Invalid template id" , "data"=>[]);
             return json_encode($responseJson);
         }
        }else{
            $data['templateName'] = $input->templateName;
            $data['printPreference'] = $input->printPreference;
            $data['templateHtmlFront'] = $input->templateHtmlFront;
            $data['templateHtmlBack'] = $input->templateHtmlBack;
            $data['projectId'] = $input->projectId;
            $data['templateLayout'] = $input->templateLayout;
            $templates = new Templates();
            $templates->template_name = $data['templateName'];
            $templates->print_preference = $data['printPreference'];
            $templates->template_html_front = $data['templateHtmlFront'] ;
            $templates->template_html_back = $data['templateHtmlBack'] ;
            $templates->project_id = $data['projectId'];
            $templates->template_layout = $data['templateLayout'];
            if($templates->save()){
                if(count($input->projectFields) > 0){
                    foreach($input->projectFields as $fields){
                        if(isset($fields->projectFieldId)){
                            $projectFields = ProjectFields::where('project_field_id', $fields->projectFieldId)->get()->first();
                            //$projectFields->project_id = $projectFields->project_id;
                            $projectFields->project_field_name = $fields->projectFieldName;
                            $projectFields->project_field_type = $fields->projectFieldType;
                            $projectFields->max_length = $fields->projectFieldMaxLength;
                            $projectFields->save();
                        }
                    }
                 }
                $templates = Templates::where('template_id',$templates->template_id )->get()->first();
                $response = [];
                $response['templateId'] = $templates->template_id;
                $response['templateName'] = $templates->template_name;
                $response['printPreference'] = $templates->print_preference;
                $response['templateHtmlFront'] = $templates->template_html_front;
                $response['templateHtmlBack'] = $templates->template_html_back;
                $response['projectId'] = $templates->project_id;
                $response['templateLayout'] = $templates->template_layout;
                $responseJson = array("status"=>"200" , "message"=>"Templates created successfully", "data"=>$response);
                return json_encode($responseJson);
            } else {
                $responseJson = array("status"=>"250", "message"=>"Failed");
                return json_encode($responseJson);
            }      
        }
   }
   public function deleteTemplates($templateId, Request $request)
   {
       $input = json_decode(file_get_contents('php://input', true));
        try{
            DB::beginTransaction();
            $id = Templates::find($templateId);
            if($id != null) {      
                $tempPdfs = TemplatePdfs::where('template_id', $templateId)->get()->all(); 
                $tokenIds = $templateDataIds = []; 
                if(count($tempPdfs) > 0){
                    foreach($tempPdfs as $tempPdf){
                        $templateDataIds[] = $tempPdf->template_data_id;
                    }
                }     
                $templateData = TemplateData::whereIn('template_data_id', $templateDataIds)->get()->all();
                if(count($templateData) > 0){
                    foreach($templateData as $templateDat){
                        $tokenIds[] = $templateDat->token_id;
                    }
                }
                if(count($templateDataIds) > 0){
                    if(TemplatePdfs::whereIn('template_data_id', $templateDataIds)->delete()){
                        TemplateData::whereIn('template_data_id', $templateDataIds)->delete();
                    }
                }
                if(count($tokenIds) > 0){
                    Tokens::whereIn('token_id', $tokenIds)->delete();
                }
                
                if($id->delete()) {
                    DB::commit();
                    $responseJson = array("status"=>"200", "message"=>"Template deleted successfully", "data"=> $id);
                    return json_encode($responseJson);              
                }else{
                    DB::rollback();
                    $responseJson = array("status"=>"250", "message"=>"Failed ", "data"=>[]);
                    return json_encode($responseJson);              
                }
            }else {
                DB::rollback();
                $responseJson = array("status"=>"250", "message"=>"Invalid template id ", "data"=>[]);
                return json_encode($responseJson); 
            }  
        }catch(\Exception $e){
            DB::rollback();
        }
    }
    public function getProjectFieldsByToken($token, Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));

        $tokenData = Tokens::where('token', $token)->get()->first(); 
        if($tokenData != null){
            if($tokenData->isActive == 0){
                $responseJson = array("status"=>"450", "message"=> "Token is already used, please request for new token", "data"=>[] );
                return json_encode($responseJson);  
            }
        
        $response = $responsetemp = $fieldsArr = [];
        $fields = ProjectFields::where('project_id', $tokenData->project_id)->get()->all();
        $projectD = Project::where('project_id', $tokenData->project_id)->get()->first();
        //$organizationId = Project::where('project_id', $tokenData->project_id)->get()->first();
        $organizationD = Organization::where('organization_id', $projectD->organization_id)->get()->first();
        if(!empty($fields)){
            foreach($fields as $key => $types){
                    $fieldsArr = []; //$dropdownArr = [];
                    $fieldsArr['projectFieldId'] = $types->project_field_id;
                    $fieldsArr['projectFieldName'] = $types->project_field_name;
                    $fieldsArr['projectFieldType'] = $types->project_field_type;
                    $fieldsArr['projectFieldMaxLength'] = $types->max_length;
                    $fieldsArr['projectId'] = $types->project_id;
                    
                    $projectFieldDropdown = ProjectFieldDropdown::where('project_field_id',$types->project_field_id)->get()->all();
                    $dropdownArr = [];
                    if(count($projectFieldDropdown) > 0){
                        foreach($projectFieldDropdown as $projectFieldDropdown){
                            $dropdownArr[] = $projectFieldDropdown->name;
                        }
                    }
                    $fieldsArr['options'] = $dropdownArr;  
                    $response[] = $fieldsArr;
                }
            }
            $templateArr = [];
            $template = Templates::where('project_id', $tokenData->project_id)->get()->all();
            if(!empty($template)){
                foreach($template as $key => $types){
                        $templateArr = [];
                        $templateArr['templateId'] = $types->template_id;
                        $templateArr['templateName'] = $types->template_name;
                        $templateArr['width'] = $types->width;
                        $templateArr['height'] = $types->height;
                        $templateArr['printPreference'] = $types->print_preference;
                        $templateArr['projectId'] = $types->project_id;
                        $templateArr['templateHtmlFront'] = $types->template_html_front;
                        $templateArr['templateHtmlBack'] = $types->template_html_back;
                        $templateArr['templateLayout'] = $types->template_layout;
                        $responsetemp[] = $templateArr;
                    }
                }
                if(count($response) > 0 OR count($responsetemp) > 0){
                    $responseJson = array("status"=>"200", "message"=>"Successful", "data"=> ["projectId" => $projectD->project_id, "organizationId" => $organizationD->organization_id, "tokenId" => $tokenData->token_id, "organizationName" => $organizationD->organization_name, "projectName" => $projectD->project_name, "projectField" => $response, "templates" => $responsetemp, "paymentType" => $projectD->payment_type, "amount" => $projectD->amount]);
                    return json_encode($responseJson);  
                }else{
                    $responseJson = array("status"=>"200", "message"=>"Successful", "data"=>[]);
                    return json_encode($responseJson);   
                }              
        }else {
            $responseJson = array("status"=>"250", "message"=>"Invalid token", "data"=>[]);
            return json_encode($responseJson);              
        }
    }
    public function getTemplates($projectId,Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $templateArr = [];
        $template = Templates::where('project_id', $projectId)->get()->all();
        if($template != null){

        foreach($template as $key => $types){
                $templateArr = [];
                $templateArr['templateId'] = $types->template_id;
                $templateArr['templateName'] = $types->template_name;
                $templateArr['width'] = $types->width;
                $templateArr['height'] = $types->height;
                $templateArr['printPreference'] = $types->print_preference;
                $templateArr['projectId'] = $types->project_id;
                $templateArr['templateHtmlFront'] = $types->template_html_front;
                $templateArr['templateHtmlBack'] = $types->template_html_back;
                $templateArr['templateLayout'] = $types->template_layout;
                $responsetemp[] = $templateArr;
            }
        if(count($responsetemp) > 0) {
            $responseJson = array("status" => "200", "message" => "Successful", "data" => $responsetemp);
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status" => "200", "message" => "Successful", "data" => []);
            return json_encode($responseJson);              
        } } else {
            $responseJson = array("status" => "200", "message" => "Success", "data" => []);
            return json_encode($responseJson);              
        }
    }
    public function saveTemplateData($templateDataId, Request $request){
        $input = json_decode(file_get_contents('php://input', true));

        if($templateDataId != 0){
            $checkAuth = $this->getAuthenticatedUser($request);
            if(json_decode($checkAuth)->status == 200){
                $templateData = TemplateData::where('template_data_id', $templateDataId)->get()->first();
                $organizationsData = Organization::where('organization_id', $input->organizationId)->get()->first();
                $projectssData = Project::where('project_id', $input->projectId)->get()->first();
                if($templateData != null){
                    $tempId['templateDataId'] = $templateData->template_data_id;
                    $templateData->message = $data['message'] = $input->message;
                    $templateData->status = $data['status'] = $input->status;
                    $templateData->token_id = $data['tokenId'] = $input->tokenId;
                    $templateData->updated_at = $data['updatedAt'] = date("Y-m-d H:i:s");
                    $dataArr = json_decode($templateData->data);
                    if($templateData->save()){
                        $subject = ' New Submission from '.$projectssData->project_name;
                        $files = [];
                        $emailAndbody = [];
                        $emailAndbody[0]['email'] = $dataArr->Email;
                        $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $input->projectId);
                        $status = $this->sendMail($subject, $emailAndbody, $files);
                        if($input->status == 'Rejected'){
                            $tokens = Tokens::where('token_id', $input->tokenId)->where('project_id', $input->projectId)->get()->first();
                            $oldToken = $tokens->token;
                                $var = $this->getToken($input->projectId);
                                $tokens->token_id = $input->tokenId;
                                $tokens->token = $var;
                                $tokens->isActive = '1';
                                $tokens->project_id = $input->projectId;
                                if($tokens->save()){
                                    $subject = 'Token Rejection';
                                    $files = [];
                                    $emailAndbodyOrg = $emailAndbody = [];
                                    $emailAndbody[0]['email'] = $dataArr->Email;
                                    $emailAndbody[0]['html'] = 'Dear '.$dataArr->Name.',
                                    Your ID card against token '.$oldToken.' has been rejected due to '.$input->message.'. Please re-enter your details using this token '.$var.'. If you are facing any issue please contact your organization on '.$organizationsData->organization_email.'.
                            
                            Regards,
                            Golden Lamtouch';
                                    $status = $this->sendMail($subject, $emailAndbody, $files);

                                    $emailAndbodyOrg[0]['email'] = $organizationsData->organization_email;
                                    $emailAndbodyOrg[0]['html'] = 'Token '.$oldToken.' has been rejected due to '.$input->message.'. Please use this token '.$var.'.';
                                    $status = $this->sendMail($subject, $emailAndbodyOrg, $files);

                                }else{
                                    $responseJson = array("status"=>"250", "message"=>"Updation of Token failed.");
                                    return json_encode($responseJson);
                                }
                        }elseif($input->status == 'Accepted'){
                            $subject = ' New Submission from '.$projectssData->project_name;
                            $files = [];
                            // $emailAndbody = [];
                            // $emailAndbody[0]['email'] = $dataArr->Email;
                            // $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $input->projectId);
                            // $status = $this->sendMail($subject, $emailAndbody, $files);

                            $emailAndbody = [];
                            $emailAndbody[0]['email'] = $organizationsData->organization_email;
                            $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $input->projectId);
                            $status = $this->sendMail($subject, $emailAndbody, $files);
                        if($input->paymentType == 'Postpaid'){
                            $usedToken = Tokens::where('project_id', $input->projectId)->where('print_no', '!=' , 0)->get();
                            $usedTokenCount = $usedToken->count();
                            $usedTokenCount = $usedTokenCount++;
                            $token = Tokens::where('token_id', $input->tokenId)->get()->first();
                            $token->isActive = 0;
                            $token->print_no = $input->projectId.'00'.$usedTokenCount+1;
                            $token->save();
                        }
                        if($input->paymentType == 'Prepaid'){
                            $payments = Payment::where('token_id', $input->tokenId)->get()->first();
                            if($payments != null){
                                if($payments->payment_status == 1){
                                    $responseJson = array("status"=>"200", "message"=>"Success.", "data" => []);
                                    return json_encode($responseJson);
                                }
                            }
                            $orderPayment = $this->createPayment($input->amount, $input->tokenId);
                            if($orderPayment['status'] == '200'){
                                $responseJson = array("status" => "200" , "message" => "TemplateData created successfully", "data" => $orderPayment['data']);
                                return json_encode($responseJson);
                            }else{
                                $responseJson = array("status" => "250" , "message" => "Creation of order id failed");
                                return json_encode($responseJson);
                            }
                        }
                        }
                        $responseJson = array("status"=>"200", "message"=>"Success.", "data" => []);
                        return json_encode($responseJson);
                    }else{
                        $responseJson = array("status"=>"250", "message"=>"Updation of Tempaltedata failed.");
                        return json_encode($responseJson);
                    } 
                }else{
                    $responseJson = array("status"=>"250", "message"=>"Invalid templatdata id" , "data"=>[]);
                    return json_encode($responseJson);
                    }
            }else{
                return $checkAuth;
            }
        }else{
            $organizationsData = Organization::where('organization_id', $input->organizationId)->get()->first();
            $projectssData = Project::where('project_id', $input->projectId)->get()->first();
            $data['organizationId'] = $input->organizationId;
            $data['projectId'] = $input->projectId;
            $data['data'] = $input->data;
            $data['tokenId'] = $input->tokenId;
            $data['status'] = $input->status;
            $data['userImage'] = $input->userImage;
            $data['message'] = $input->message;
            $data['paymentType'] = $input->paymentType;
            $data['pdfData'] = $input->pdfData;
            $data['createdAt'] = date("Y-m-d H:i:s");
            $tokenId = Tokens::where('token_id', $data['tokenId'])->get()->first();
            if($tokenId != null){
                $templateData = TemplateData::where('token_id', $input->tokenId)->get()->first();
                if($templateData != null){
                    $templateData->data = json_encode($input->data);
                    $templateData->organization_id = $input->organizationId;
                    $templateData->project_id = $input->projectId;
                    $templateData->token_id = $input->tokenId;
                    $templateData->status = $input->status;
                    $templateData->user_image = $input->userImage;
                    $templateData->message = $input->message;
                    $templateData->created_at = date("Y-m-d H:i:s");
                    if($templateData->save()){
                        $dataArr = $input->data;
                        $subject = ' New Submission from '.$projectssData->project_name;
                        $files = [];
                        $emailAndbody = [];
                        $emailAndbody[0]['email'] = $dataArr->Email;
                        $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $input->projectId);
                        $status = $this->sendMail($subject, $emailAndbody, $files);

                        $tempId['templateDataId'] = $templateData->template_data_id;
                        $tempPdfs = [];
                        if(count($data['pdfData']) > 0){
                            TemplatePdfs::where('template_data_id', $templateData->template_data_id)->delete();
                            foreach($data['pdfData'] as $da){
                                if(count($da->links) > 0){
                                    foreach($da->links as $link){
                                        $tempPdfs[] = array('template_pdf_id' => null, 'pdf_link' => $link, 'template_id' => $da->templateId, 'template_data_id' => $templateData->template_data_id);
                                    }
                                }
                            }
                            if(count($tempPdfs) > 0){
                                if(!TemplatePdfs::insert($tempPdfs)){
                                    $responseJson = array("status" => "250" , "message" => "Links of ids not saved.");
                                    return json_encode($responseJson);
                                }
                            }
                        }

                        if($input->status == 'Accepted'){
                            $dataArr = $input->data;
                            $subject = ' New Submission from '.$projectssData->project_name;
                            $files = [];
                            // $emailAndbody = [];
                            // $emailAndbody[0]['email'] = $dataArr->Email;
                            // $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $input->projectId);
                            // $status = $this->sendMail($subject, $emailAndbody, $files);

                            $emailAndbody = [];
                            $emailAndbody[0]['email'] = $organizationsData->organization_email;
                            $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $input->projectId);
                            $status = $this->sendMail($subject, $emailAndbody, $files);
                        }
                        if($input->paymentType == 'Postpaid'){
                            $usedToken = Tokens::where('project_id', $input->projectId)->where('print_no', '!=' , 0)->get();
                            $usedTokenCount = $usedToken->count();
                            $usedTokenCount = $usedTokenCount++;
                            $token = Tokens::where('token_id', $data['tokenId'])->get()->first();
                            $token->isActive = 0;
                            $token->print_no = $input->projectId.'00'.$usedTokenCount+1;
                            $token->save();
                        }
                        if($input->paymentType == 'Prepaid'){
                            
                            $payments = Payment::where('token_id', $input->tokenId)->get()->first();
                            if($payments != null){
                                if($payments->payment_status == 1){
                                    $responseJson = array("status" => "200" , "message" => "TemplateData created successfully", "data" => $data);
                                    return json_encode($responseJson);
                                }
                            }
                            $orderPayment = $this->createPayment($input->amount, $input->tokenId);
                            if($orderPayment['status'] == '200'){
                                $responseJson = array("status" => "200" , "message" => "TemplateData created successfully", "data" => $orderPayment['data']);
                                return json_encode($responseJson);
                            }else{
                                $responseJson = array("status" => "250" , "message" => "Creation of order id failed");
                                return json_encode($responseJson);
                            }
                        }
                        $responseJson = array("status" => "200" , "message" => "TemplateData created successfully", "data" => $data);
                        return json_encode($responseJson);
                    } else {
                        $responseJson = array("status"=>"250", "message"=>"Failed", "data"=>[]);
                        return json_encode($responseJson);
                    }
                }else{
                    $templateData = new TemplateData();
                    $templateData->data = json_encode($data['data']);
                    $templateData->organization_id = $data['organizationId'];
                    $templateData->project_id = $data['projectId'];
                    $templateData->token_id = $data['tokenId'];
                    $templateData->status = $data['status'];
                    $templateData->user_image = $data['userImage'];
                    $templateData->message = $data['message'];
                    $templateData->created_at = $data['createdAt'];
                    if($templateData->save()){

                        $dataArr = $data['data'];
                        $subject = ' New Submission from '.$projectssData->project_name;
                        $files = [];
                        $emailAndbody = [];
                        $emailAndbody[0]['email'] = $dataArr->Email;
                        $emailAndbody[0]['html'] = $this->getSuccessEmailContent($data['data'], $data['projectId']);
                        $status = $this->sendMail($subject, $emailAndbody, $files);

                        $tempId['templateDataId'] = $templateData->template_data_id;
                        $tempPdfs = [];
                        if(count($data['pdfData']) > 0){
                            foreach($data['pdfData'] as $da){
                                if(count($da->links) > 0){
                                    foreach($da->links as $link){
                                        $tempPdfs[] = array('template_pdf_id' => null, 'pdf_link' => $link, 'template_id' => $da->templateId, 'template_data_id' => $templateData->template_data_id);
                                    }
                                }
                            }
                            if(count($tempPdfs) > 0){
                                if(!TemplatePdfs::insert($tempPdfs)){
                                    $responseJson = array("status" => "250" , "message" => "Links of ids not saved.");
                                    return json_encode($responseJson);
                                }
                            }
                        }
                        if($data['status'] == 'Accepted'){
                            $dataArr = $data['data'];
                            $subject = ' New Submission from '.$projectssData->project_name;
                            $files = [];
                            // $emailAndbody = [];
                            // $emailAndbody[0]['email'] = $dataArr->Email;
                            // $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $data['projectId']);
                            // $status = $this->sendMail($subject, $emailAndbody, $files);

                            $emailAndbody = [];
                            $emailAndbody[0]['email'] = $organizationsData->organization_email;
                            $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $data['projectId']);
                            $status = $this->sendMail($subject, $emailAndbody, $files);
                        }
                        if($input->paymentType == 'Postpaid'){
                            $usedToken = Tokens::where('project_id', $data['projectId'])->where('print_no', '!=' , 0)->get();
                            $usedTokenCount = $usedToken->count();
                            $usedTokenCount = $usedTokenCount++;
                            $token = Tokens::where('token_id', $data['tokenId'])->get()->first();
                            $token->isActive = 0;
                            $token->print_no = $data['projectId'].'00'.$usedTokenCount+1;
                            $token->save();
                        }
                        if($input->paymentType == 'Prepaid'){
                            
                            $payments = Payment::where('token_id', $input->tokenId)->get()->first();
                            if($payments != null){
                                if($payments->payment_status == 1){
                                    $responseJson = array("status" => "200" , "message" => "TemplateData created successfully", "data" => $data);
                                    return json_encode($responseJson);
                                }
                            }
                            $orderPayment = $this->createPayment($input->amount, $input->tokenId);
                            if($orderPayment['status'] == '200'){
                                $responseJson = array("status" => "200" , "message" => "TemplateData created successfully", "data" => $orderPayment['data']);
                                return json_encode($responseJson);
                            }else{
                                $responseJson = array("status" => "250" , "message" => "Creation of order id failed");
                                return json_encode($responseJson);
                            }
                        }
                        $responseJson = array("status" => "200" , "message" => "TemplateData created successfully", "data" => $data);
                        return json_encode($responseJson);
                    } else {
                        $responseJson = array("status"=>"250", "message"=>"Failed", "data"=>[]);
                        return json_encode($responseJson);
                    }
                }
            }else{ 
                $responseJson = array("status"=>"250" , "message"=>"Invalid token id","data"=>[]);
                return json_encode($responseJson);
            }     
        }
    }
    public function getAllTemplateData( Request $request){
        $input = json_decode(file_get_contents('php://input', true));
        
        $templateDataArr = $templatesArr = $response = [];
        $templateData = TemplateData::get()->all();
        $templates = Templates::get()->all();
        if(count($templates) > 0){
            foreach($templates as $template){
                $templatesArr[$template->template_id] = $template->template_name;
            }
        }

        $projectData = Project::get()->all();
        $organizationData = Organization::get()->all();
        $tokenData = Tokens::get()->all();
        $projectNameArr = $organizatinoNameArr = $tokenArr = [];
        if(count($projectData) > 0){
            foreach($projectData as $project){
                $projectNameArr[$project->project_id] = $project->project_name;
            }
        }
        if(count($organizationData) > 0){
            foreach($organizationData as $organization){
                $organizatinoNameArr[$organization->organization_id] = $organization->organization_name;
            }
        }
        $tokenPrNoArr = $tokenSrNoArr = [];
        if(count($tokenData) > 0){
            foreach($tokenData as $token){
                $tokenArr[$token->token_id] = $token->token;
                $tokenSrNoArr[$token->token_id] = $token->serial_no;
                $tokenPrNoArr[$token->token_id] = $token->print_no;
            }
        }

        foreach($templateData as $key => $types){
            $templateDataArr = [];
            $templateDataArr['templateDataId'] = $types->template_data_id;
            $templateDataArr['projectId'] = $types->project_id;
            $templateDataArr['organizationId'] = $types->organization_id;
            $templateDataArr['data'] = json_decode($types->data);
            $templateDataArr['tokenId'] = $types->token_id;
            $templateDataArr['tokenSerialNo'] = $tokenSrNoArr[$types->token_id];
            $templateDataArr['tokenPrintNo'] = $tokenPrNoArr[$types->token_id];
            $templateDataArr['status'] = $types->status;
            $templateDataArr['userImage'] = $types->user_image;
            $templateDataArr['message'] = $types->message;
            $templateDataArr['projectName'] = $projectNameArr[$types->project_id];
            $templateDataArr['organizationName'] = $organizatinoNameArr[$types->organization_id];
            $templateDataArr['token'] = $tokenArr[$types->token_id];
            $templateDataArr['updatedAt'] = ($types->updated_at != null) ? ($types->updated_at) : ($types->created_at);
            $templateDataArr['createdAt'] = $types->created_at;
            $finalArr = $tempArr = [];
            $tempPdfs = TemplatePdfs::where('template_data_id', $types->template_data_id)->get()->all();
            if(count($tempPdfs) > 0){
                foreach($tempPdfs as $tempPdf){
                    $tempArr[$tempPdf->template_id][] = $tempPdf->pdf_link;
                }
            }
            
            if(count($tempArr) > 0){
                foreach($tempArr as $temp => $tempval){
                    $tempPdfsArr = [];
                    $tempPdfsArr['templateName'] = $templatesArr[$temp];
                    $tempPdfsArr['templateId'] = $temp;
                    $tempPdfsArr['links'] = $tempval;
                    $finalArr[] = $tempPdfsArr;
                }
            }
            $templateDataArr['pdfData'] = $finalArr;
            $response[] = $templateDataArr;
        }
        if(count($response) > 0) {
            $responseJson = array("status" => "200", "message" => "Success", "data" => $response);
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status" => "200", "message" => "Success", "data" => []);
            return json_encode($responseJson);              
        }
    }
    public function importExcel(Request $request){
        $input = json_decode(file_get_contents('php://input', true));   
        $extensions = array("xls","xlsx");
        $result = array($request->file('file')->getClientOriginalExtension());
        
        if(in_array($result[0],$extensions)){
            Excel::import(new TemplateDataImport,request()->file('file'));
            $responseJson = array("status" => "200", "message" => "Excel uploaded successfully", "data" =>[] );
            return json_encode($responseJson);    
        }else{
            $responseJson = array("status" => "250", "message" => "Required File must be type of xlsx and xls", "data" => []);
            return json_encode($responseJson);  
        }
    }
    public function getTemplateDatabyProject($projectId, Request $request){
        $input = json_decode(file_get_contents('php://input', true));
        $templatesLayoutArr = $templatesPrintPreferenceArr = $templateDataArr = $templatesArr = $response = [];
        $templateData = TemplateData::where('project_id', $projectId)->get()->all();

        $templates = Templates::where('project_id', $projectId)->get()->all();
        if(count($templates) > 0){
            foreach($templates as $template){
                $templatesArr[$template->template_id] = $template->template_name;
                $templatesLayoutArr[$template->template_id] = $template->template_layout;
                $templatesPrintPreferenceArr[$template->template_id] = $template->print_preference;
            }
        }

        $projectData = Project::get()->all();
        $organizationData = Organization::get()->all();
        $tokenData = Tokens::get()->all();
        $projectNameArr = $organizatinoNameArr = $tokenArr = [];
        if(count($projectData) > 0){
            foreach($projectData as $project){
                $projectNameArr[$project->project_id] = $project->project_name;
            }
        }
        if(count($organizationData) > 0){
            foreach($organizationData as $organization){
                $organizatinoNameArr[$organization->organization_id] = $organization->organization_name;
            }
        }
        $tokenPrNoArr = $tokenSrNoArr = [];
        if(count($tokenData) > 0){
            foreach($tokenData as $token){
                $tokenArr[$token->token_id] = $token->token;
                $tokenSrNoArr[$token->token_id] = $token->serial_no;
                $tokenPrNoArr[$token->token_id] = $token->print_no;
            }
        }

        foreach($templateData as $key => $types){
            $templateDataArr = [];
            $templateDataArr['templateDataId'] = $types->template_data_id;
            $templateDataArr['projectId'] = $types->project_id;
            $templateDataArr['organizationId'] = $types->organization_id;
            $templateDataArr['data'] = json_decode($types->data);
            $templateDataArr['tokenId'] = $types->token_id;
            $templateDataArr['tokenSerialNo'] = $tokenSrNoArr[$types->token_id];
            $templateDataArr['tokenPrintNo'] = $tokenPrNoArr[$types->token_id];
            $templateDataArr['status'] = $types->status;
            $templateDataArr['userImage'] = $types->user_image;
            $templateDataArr['message'] = $types->message;
            $templateDataArr['projectName'] = $projectNameArr[$types->project_id];
            $templateDataArr['organizationName'] = $organizatinoNameArr[$types->organization_id];
            $templateDataArr['token'] = $tokenArr[$types->token_id];
            $templateDataArr['updatedAt'] = ($types->updated_at != null) ? ($types->updated_at) : ($types->created_at);
            $templateDataArr['createdAt'] = $types->created_at;

            $finalArr = $tempArr = [];
            $tempPdfs = TemplatePdfs::where('template_data_id', $types->template_data_id)->get()->all();
            if(count($tempPdfs) > 0){
                foreach($tempPdfs as $tempPdf){
                    $tempArr[$tempPdf->template_id][] = $tempPdf->pdf_link;
                }
            }
            
            if(count($tempArr) > 0){
                foreach($tempArr as $temp => $tempval){
                    $tempPdfsArr = [];
                    $tempPdfsArr['templatePrintPreference'] = $templatesPrintPreferenceArr[$temp];
                    $tempPdfsArr['templateName'] = $templatesArr[$temp];
                    $tempPdfsArr['templateId'] = $temp;
                    $tempPdfsArr['links'] = $tempval;
                    $tempPdfsArr['templateLayout'] = $templatesLayoutArr[$temp];
                    $finalArr[] = $tempPdfsArr;
                }
            }
            $templateDataArr['pdfData'] = $finalArr;
            
            $response[] = $templateDataArr;
        }
        if(count($response) > 0) {
            $responseJson = array("status" => "200", "message" => "Success", "data" => $response);
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status" => "200", "message" => "Success", "data" => []);
            return json_encode($responseJson);              
        }
    }

    public function getAuthenticatedUser(Request $request)
    {
        $response = auth('api')->check();
        $responseCode = 200;
        $responseMsg = 'Success.';
        if(!$response) {
            try {
               if (!app(\Tymon\JWTAuth\JWTAuth::class)->parseToken()->authenticate()) {
                $response = 0;
                $responseCode = 404;
                $responseMsg = 'User not found.';
               }
            } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
               $response = -1;
               $responseCode = 401;
               $responseMsg = 'Token Expired.';
            } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
               $response = -2;
               $responseCode = 403;
               $responseMsg = 'Token Invalid.';
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
               $response = -3;
               $responseCode = 500;
               $responseMsg = 'Token Absent.';
            }
        } else {
            $response = (int) $response;
        }
        $responseJson = array("status" => $responseCode, "message" => $responseMsg, "data" => []);
        return json_encode($responseJson); 
    }

    public function getDashboardData(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        try {
            $status1 = 'Accepted';
            $status2 = 'Rejected';
            $status3 = 'InProgress'; 

            $jobStatus1 = 'Completed';
            $jobStatus2 = 'InProgress';
            $jobStatus3 = 'Failed'; 

            $templateData = Templates::get()->all();
            $projectData = Project::get()->all();
            $organizationData = Organization::get()->all();
            $tokenData = Tokens::get()->all();
            $acceptedTokenData = Tokens::where('status', $status1)->join('template_data', 'template_data.token_id', 'tokens.token_id')->get()->all();
            $rejectedTokenData = Tokens::where('status', $status2)->join('template_data', 'template_data.token_id', 'tokens.token_id')->get()->all();
            $printJobsData = PrintJobs::get()->all();
            $printJobsCompletedData = PrintJobs::where('status', $jobStatus1)->get()->all();
            $printJobsInprogressData = PrintJobs::where('status', $jobStatus2)->get()->all();
            $printJobsFailedData = PrintJobs::where('status', $jobStatus3)->get()->all();

            $lastMonthPrintJobsCompletedData = PrintJobs::where('status', $jobStatus1)->whereMonth('created_at', '<=', Carbon::now()->subMonth()->month)->get()->all();
            $todaysPrintJobsCompletedData = PrintJobs::where('status', $jobStatus1)->whereDate('created_at', Carbon::today())->get()->all();
            $lastSixMonthsPrintJobsCompletedData = PrintJobs::where('status', $jobStatus1)->where("created_at",">", Carbon::now()->subMonths(6))->get()->all();
            $lastYearsPrintJobsCompletedData = PrintJobs::where('status', $jobStatus1)->whereDate('created_at','>', now()->subYear())->get()->all();
            
            $response = array('totalTemplates' => count($templateData), 'totalProjects' => count($projectData), 'totalOrganizations' => count($organizationData), 'totalTokens' => count($tokenData), 'acceptedTokenTotal' => count($acceptedTokenData), 'rejectedTokenTotal' => count($rejectedTokenData), 'printJobsCompletedData' => count($printJobsCompletedData), 'printJobsInprogressData' => count($printJobsInprogressData), 'printJobsFailedData' => count($printJobsFailedData), 'totalPrintJobs' => count($printJobsData), 'lastMonthPrintJobsCompletedData' => count($lastMonthPrintJobsCompletedData), 'todaysPrintJobsCompletedData' => count($todaysPrintJobsCompletedData), 'lastSixMonthsPrintJobsCompletedData' => count($lastSixMonthsPrintJobsCompletedData), 'lastYearsPrintJobsCompletedData' => count($lastYearsPrintJobsCompletedData));

            $responseJson = array("status" => "200", "message" => "Success", "data" => $response);
            return json_encode($responseJson); 
        } catch (Exception $e) {
            $response = json_encode(array("status" => 150, "message" => $e->getMessage(), "data" => []));
            return $response; 
        }
    }
    public function getYearWisePrintJobs($year){
        
        //$jobs = PrintJobs::whereYear('created_at', '=', $year)->get();
        $jobs = DB::select("SELECT YEAR(created_at) AS y, MONTH(created_at) AS m, COUNT(DISTINCT print_job_id) as c FROM print_jobs WHERE YEAR(created_at) = ".$year." GROUP BY y, m");
        
        if(count($jobs) > 0){
            $printJobs = [];
            foreach($jobs as $job){
                $printJobs[$job->m] = $job->c;
            }
            $responseJson = array("status" => "200", "message" => "Success", "data" => $printJobs);
            return json_encode($responseJson); 
        }else{
            $responseJson = array("status" => "200", "message" => "Success", "data" => []);
            return json_encode($responseJson); 
        }
    }
    public function createPayment($amount, $tokenId)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        $gst = 0;
        $tokenRow = Tokens::where('token_id', $tokenId)->get()->first();
        if($tokenRow != null){
            $projectRow = Project::where('project_id', $tokenRow->project_id)->get()->first();
            if($projectRow != null){
                if($projectRow->with_or_without_gst == 1){
                    $appConfigRow = AppConfig::where('config', 'charged_percent')->get()->first();
                    if($appConfigRow != null){
                        $gst = $appConfigRow->value;
                    }
                }
            }
        }
        $amount = $amount;
        $exAmount = 0;
        if($gst > 0){
            $gst1 = $gst*$amount;
            $exAmount = $gst1/100;
            $amount = $amount + $exAmount;
        }
        $amount = strval($amount);
        $order  = $api->order->create(array('receipt' => $tokenId, 'amount' => $amount*100, 'currency' => 'INR')); // Creates order
        $orderId = $order['id']; 

        $user_pay = new Payment();
        $user_pay->amount = $amount;
        $user_pay->token_id = $tokenId;
        $user_pay->order_id = $orderId;
        $user_pay->created_at = date("Y-m-d H:i:s");
        if($user_pay->save()){
            $data = array(
            'order_id' => $orderId,
            'amount' => $amount,
            'tokenId' => $tokenId,
            'key' => env('RAZORPAY_KEY'),
            'paymentId' => $user_pay->payment_id
        );
            return $responseJson = array("status" => "200", "message" => "Success", "data" => $data);
            //return json_encode($responseJson); 
        }else{
            return $responseJson = array("status" => "250", "message" => "Failed", "data" => []);
            //return json_encode($responseJson); 
        }
    }
    public function donePayment(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        
        if($input->paymentStatus == 1){
            $token = Tokens::where('token_id', $input->tokenId)->get()->first();
            $usedToken = Tokens::where('project_id', $token->project_id)->where('print_no', '!=' , 0)->get();
            $usedTokenCount = $usedToken->count();
            $usedTokenCount = $usedTokenCount++;
            $token->isActive = 0;
            $token->print_no = $token->project_id.'00'.$usedTokenCount+1;
            $token->save();
        }
        $user_pay = Payment::find($input->paymentId);
        $user_pay->razorpay_id = $input->razorpayId;
        $user_pay->payment_status = $input->paymentStatus;
        $user_pay->updated_at = date("Y-m-d H:i:s");
        if($user_pay->save()){
            $responseJson = array("status" => "200", "message" => "Success");
            return json_encode($responseJson); 
        }else{
            $responseJson = array("status" => "200", "message" => "Success", "data" => []);
            return json_encode($responseJson); 
        }
    }
    public function getSubmissions(Request $request)
    {
        $input = json_decode(file_get_contents('php://input', true));
        $response =[];
        $status1 = 'Accepted';
        $status2 = 'Rejected';
        $status3 = 'InProgress'; 
        $project = DB::table('projects')
       ->join('organizations', 'projects.organization_id', '=', 'organizations.organization_id')
        ->select('projects.project_id','projects.project_name','projects.description','projects.no_of_tokens',
             'projects.organization_id', 'organizations.organization_name','projects.payment_type','projects.amount','projects.with_or_without_gst')
              ->get()->all();

            $response = [];
            foreach($project as $key => $types){

                $tokens = Tokens::where('project_id', $types->project_id)->where('isActive', 0)->get()->all();
                $tokensInProgress = TemplateData::where('project_id', $types->project_id)->where('status', $status3)->get()->all();
                $tokensRejected = TemplateData::where('project_id', $types->project_id)->where('status', $status2)->get()->all();
                $tokensAccepted = TemplateData::where('project_id', $types->project_id)->where('status', $status1)->get()->all();
                $tokensSubmissions = TemplateData::where('project_id', $types->project_id)->get()->all();

                $projectArr = [];
                $projectArr['projectId'] = $types->project_id;
                $projectArr['projectName'] = $types->project_name;
                $projectArr['noOfTokens'] = $types->no_of_tokens;
                $projectArr['organizationName'] = $types->organization_name;
                $projectArr['totalSubmissions'] = count($tokensSubmissions);
                $projectArr['acceptedSubmissions'] = count($tokensAccepted);
                $projectArr['rejectedSubmissions'] = count($tokensRejected);
                $projectArr['inprogressSubmissions'] = count($tokensInProgress);
                $response[] = $projectArr;  
            }
    
        if(count($response) > 0) {
            $responseJson = array("status"=>"200", "message"=>"successful", "data"=> $response );
            return json_encode($responseJson);              
        } else {
            $responseJson = array("status"=>"200", "message"=>"successful", "data"=>[]);
            return json_encode($responseJson);              
        }
    }

    public function saveSubmission(Request $request){
        $input = json_decode(file_get_contents('php://input', true));

        $templateData = TemplateData::where('template_data_id', $input->templateDataId)->get()->first();
        $organizationsData = Organization::where('organization_id', $input->organizationId)->get()->first();
        $projectssData = Project::where('project_id', $input->projectId)->get()->first();
        if($templateData != null){
            $tempId['templateDataId'] = $templateData->template_data_id;
            $templateData->message = $data['message'] = $input->message;
            $templateData->status = $data['status'] = $input->status;
            $templateData->token_id = $data['tokenId'] = $input->tokenId;
            $templateData->updated_at = $data['updatedAt'] = date("Y-m-d H:i:s");
            $dataArr = json_decode($templateData->data);
            if($templateData->save()){
                $subject = ' New Submission from '.$projectssData->project_name;
                $files = [];
                $emailAndbody = [];
                $emailAndbody[0]['email'] = $dataArr->Email;
                $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $input->projectId);
                $status = $this->sendMail($subject, $emailAndbody, $files);
                if($input->status == 'Rejected'){
                    $tokens = Tokens::where('token_id', $input->tokenId)->where('project_id', $input->projectId)->get()->first();
                    $oldToken = $tokens->token;
                        $var = $this->getToken($input->projectId);
                        $tokens->token_id = $input->tokenId;
                        $tokens->token = $var;
                        $tokens->isActive = '1';
                        $tokens->project_id = $input->projectId;
                        if($tokens->save()){
                            $subject = 'Token Rejection';
                            $files = [];
                            $emailAndbodyOrg = $emailAndbody = [];
                            $emailAndbody[0]['email'] = $dataArr->Email;
                            $emailAndbody[0]['html'] = 'Dear '.$dataArr->Name.',
                            Your ID card against token '.$oldToken.' has been rejected due to '.$input->message.'. Please re-enter your details using this token '.$var.'. If you are facing any issue please contact your organization on '.$organizationsData->organization_email.'.
                    
                    Regards,
                    Golden Lamtouch';
                            $status = $this->sendMail($subject, $emailAndbody, $files);

                            $emailAndbodyOrg[0]['email'] = $organizationsData->organization_email;
                            $emailAndbodyOrg[0]['html'] = 'Token '.$oldToken.' has been rejected due to '.$input->message.'. Please use this token '.$var.'.';
                            $status = $this->sendMail($subject, $emailAndbodyOrg, $files);

                        }else{
                            $responseJson = array("status"=>"250", "message"=>"Updation of Token failed.");
                            return json_encode($responseJson);
                        }
                }elseif($input->status == 'Accepted'){
                    $subject = ' New Submission from '.$projectssData->project_name;
                    $files = [];
                        // $emailAndbody = [];
                        // $emailAndbody[0]['email'] = $dataArr->Email;
                        // $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $input->projectId);
                        // $status = $this->sendMail($subject, $emailAndbody, $files);

                        $emailAndbody = [];
                        $emailAndbody[0]['email'] = $organizationsData->organization_email;
                        $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $input->projectId);
                        $status = $this->sendMail($subject, $emailAndbody, $files);
                }
                $responseJson = array("status"=>"200", "message"=>"Success.", "data" => []);
                return json_encode($responseJson);
            }else{
                $responseJson = array("status"=>"250", "message"=>"Updation of Tempaltedata failed.");
                return json_encode($responseJson);
            }
        
        }else{
            $responseJson = array("status"=>"250", "message"=>"Invalid templatdata id" , "data"=>[]);
            return json_encode($responseJson);
            }
    }
    public function saveMultipleSubmissions(Request $request){
        $input = json_decode(file_get_contents('php://input', true));
        $a = 0;
        if(count($input->data) > 0){
            foreach($input->data as $data){
                $templateData = TemplateData::where('template_data_id', $data->templateDataId)->get()->first();
                $organizationsData = Organization::where('organization_id', $data->organizationId)->get()->first();
                $projectssData = Project::where('project_id', $data->projectId)->get()->first();
                $templateData->message = $data->message;
                $templateData->status = $data->status;
                $templateData->token_id = $data->tokenId;
                $templateData->updated_at = date("Y-m-d H:i:s");
                $dataArr = json_decode($templateData->data);
                if($templateData->save()){
                    $subject = ' New Submission from '.$projectssData->project_name;
                    $files = [];
                    $emailAndbody = [];
                    $emailAndbody[0]['email'] = $dataArr->Email;
                    $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $data->projectId);
                    $status = $this->sendMail($subject, $emailAndbody, $files);
                    $a++;
                    if($data->status == 'Rejected'){
                        $tokens = Tokens::where('token_id', $data->tokenId)->where('project_id', $data->projectId)->get()->first();
                        $oldToken = $tokens->token;
                            $var = $this->getToken($data->projectId);
                            $tokens->token_id = $data->tokenId;
                            $tokens->token = $var;
                            $tokens->isActive = '1';
                            $tokens->project_id = $data->projectId;
                            if($tokens->save()){
                                $subject = 'Token Rejection';
                                $files = [];
                                $emailAndbodyOrg = $emailAndbody = [];
                                $emailAndbody[0]['email'] = $dataArr->Email;
                                $emailAndbody[0]['html'] = 'Dear '.$dataArr->Name.',
                                Your ID card against token '.$oldToken.' has been rejected due to '.$data->message.'. Please re-enter your details using this token '.$var.'. If you are facing any issue please contact your organization on '.$organizationsData->organization_email.'.
                        
                        Regards,
                        Golden Lamtouch';
                                $status = $this->sendMail($subject, $emailAndbody, $files);
    
                                $emailAndbodyOrg[0]['email'] = $organizationsData->organization_email;
                                $emailAndbodyOrg[0]['html'] = 'Token '.$oldToken.' has been rejected due to '.$data->message.'. Please use this token '.$var.'.';
                                $status = $this->sendMail($subject, $emailAndbodyOrg, $files);
    
                            }else{
                                $responseJson = array("status"=>"250", "message"=>"Updation of Token failed.");
                                return json_encode($responseJson);
                            }
                        }elseif($data->status == 'Accepted'){
                            $subject = ' New Submission from '.$projectssData->project_name;
                            $files = [];
                            // $emailAndbody = [];
                            // $emailAndbody[0]['email'] = $dataArr->Email;
                            // $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $data->projectId);
                            // $status = $this->sendMail($subject, $emailAndbody, $files);

                            $emailAndbody = [];
                            $emailAndbody[0]['email'] = $organizationsData->organization_email;
                            $emailAndbody[0]['html'] = $this->getSuccessEmailContent($dataArr, $data->projectId);
                            $status = $this->sendMail($subject, $emailAndbody, $files);
                    }
                }
            }
            
            if($a == count($input->data)){
                $responseJson = array("status"=>"200", "message"=>"Success." , "data"=>[]);
                return json_encode($responseJson);
            }else{
                $responseJson = array("status"=>"250", "message"=>"Some submission not updated." , "data"=>[]);
                return json_encode($responseJson);
            }
        }else{
            $responseJson = array("status"=>"250", "message"=>"Empty data." , "data"=>[]);
            return json_encode($responseJson);
        }
    }
    public function saveMultipleTemplateData(Request $request){
        $input = json_decode(file_get_contents('php://input', true));
        $a = 0;
        if(count($input->data) > 0){
            foreach($input->data as $data){
            $dataArr['organizationId'] = $data->organizationId;
            $dataArr['projectId'] = $data->projectId;
            $dataArr['data'] = json_encode($data->data);
            $dataArr['token'] = $data->token;
            $dataArr['status'] = $data->status;
            $dataArr['message'] = $data->message;
            $dataArr['pdfData'] = $data->pdfData;
            $dataArr['createdAt'] = date("Y-m-d H:i:s");
            $tokenArr = Tokens::where('token', $dataArr['token'])->where('project_id', $data->projectId)->get()->first();
            if($tokenArr != null){
                $templateData = TemplateData::where('token_id', $tokenArr->token_id)->get()->first();
                if($templateData != null){
                    $templateData->data = json_encode($data->data);
                    $templateData->organization_id = $data->organizationId;
                    $templateData->project_id = $data->projectId;
                    $templateData->token_id = $tokenArr->token_id;
                    $templateData->status = $data->status;
                    $templateData->message = $data->message;
                    $templateData->created_at = date("Y-m-d H:i:s");
                    if($templateData->save()){
                        $tempId['templateDataId'] = $templateData->template_data_id;
                        $tempPdfs = [];
                        if(count($dataArr['pdfData']) > 0){
                            TemplatePdfs::where('template_data_id', $templateData->template_data_id)->delete();
                            foreach($dataArr['pdfData'] as $da){
                                if(count($da->links) > 0){
                                    foreach($da->links as $link){
                                        $tempPdfs[] = array('template_pdf_id' => null, 'pdf_link' => $link, 'template_id' => $da->templateId, 'template_data_id' => $templateData->template_data_id);
                                    }
                                }
                            }
                            if(count($tempPdfs) > 0){
                                if(!TemplatePdfs::insert($tempPdfs)){
                                    $responseJson = array("status" => "250" , "message" => "Links of ids not saved.");
                                    return json_encode($responseJson);
                                }
                            }
                        }
                        $usedToken = Tokens::where('project_id', $data->projectId)->where('print_no', '!=' , 0)->get();
                        $usedTokenCount = $usedToken->count();
                        $usedTokenCount = $usedTokenCount++;
                        $token = Tokens::where('token', $dataArr['token'])->where('project_id', $data->projectId)->get()->first();
                        $token->isActive = 0;
                        $token->print_no = $data->projectId.'00'.$usedTokenCount+1;
                        $token->save();
                    }else{
                        $responseJson = array("status"=>"250", "message"=>"Updation of Template data failed.");
                        return json_encode($responseJson);
                    }
                }else{
                    $templateData = new TemplateData();
                    $templateData->data = json_encode($data->data);
                    $templateData->organization_id = $data->organizationId;
                    $templateData->project_id = $data->projectId;
                    $templateData->token_id = $tokenArr->token_id;
                    $templateData->status = $data->status;
                    $templateData->message = $data->message;
                    $templateData->created_at = date("Y-m-d H:i:s");
                    if($templateData->save()){
                        $tempPdfs = [];
                        if(count($dataArr['pdfData']) > 0){
                            foreach($dataArr['pdfData'] as $da){
                                if(count($da->links) > 0){
                                    foreach($da->links as $link){
                                        $tempPdfs[] = array('template_pdf_id' => null, 'pdf_link' => $link, 'template_id' => $da->templateId, 'template_data_id' => $templateData->template_data_id);
                                    }
                                }
                            }
                            if(count($tempPdfs) > 0){
                                if(!TemplatePdfs::insert($tempPdfs)){
                                    $responseJson = array("status" => "250" , "message" => "Links of ids not saved.");
                                    return json_encode($responseJson);
                                }
                            }
                        }
                        $usedToken = Tokens::where('project_id', $data->projectId)->where('print_no', '!=' , 0)->get();
                        $usedTokenCount = $usedToken->count();
                        $usedTokenCount = $usedTokenCount++;
                            $token = Tokens::where('token', $dataArr['token'])->where('project_id', $data->projectId)->get()->first();
                            $token->isActive = 0;
                            $token->print_no = $data->projectId.'00'.$usedTokenCount+1;
                            $token->save();
                        }else{
                            $responseJson = array("status"=>"250", "message"=>"Creation of Template data failed.");
                            return json_encode($responseJson);
                        }
                    }
                }  
                $a++;
            }
            if($a == count($input->data)){
                $responseJson = array("status"=>"200", "message"=>"Success." , "data"=>[]);
                return json_encode($responseJson);
            }else{
                $responseJson = array("status"=>"250", "message"=>"Some submission not updated." , "data"=>[]);
                return json_encode($responseJson);
            }
        }else{
            $responseJson = array("status"=>"250", "message"=>"Empty data." , "data"=>[]);
            return json_encode($responseJson);
        }   
    }

    public function getSuccessEmailContent($data, $projectId){
        
        $arrayKeys = [];
        $arrayTostr =  json_encode($data);
        $strToArray = json_decode($arrayTostr, true);
        $arrayKeys = array_keys($strToArray);
             
        $projectFields = ProjectFields::where('project_id', $projectId)->get()->all();
        $tableContent = '';
        if(count($projectFields) > 0){
            foreach($projectFields as $projectField){
                if (in_array($projectField->project_field_name, $arrayKeys)){
                    if($projectField->project_field_type == "image"){
                        $tableContent .= '<tr><td>'.$projectField->project_field_name.'</td><td><img style="width: 150px;height: 150px;" src="'.$strToArray[$projectField->project_field_name].'" alt="'.$projectField->project_field_name.'"></td></tr>';
                    }else{
                        $tableContent .= '<tr><td>'.$projectField->project_field_name.'</td><td>'.$strToArray[$projectField->project_field_name].'</td></tr>';
                    }
                }
            }
        }
        
        $content =  '';
        $content .=  '<!DOCTYPE html>
        <html>
        <head>
            <title>Welcome</title>	
        </head>
        <body>
            <div class="mailBody" style="width: 750px; margin: 20px auto;background: #fff;" >	
                <div class="mailbody1" style="margin: 15px auto;color: #000000;font-size: 14px;font-weight: 400;line-height: 20px;letter-spacing: 0em;text-align: left;border: 1px solid #DDDCDC;border-radius: 9px;padding: 20px;">
                    <p>Dear <b>Applicant</b>,</p>
        
                    <p>Thank you for sending your application/information to us. It is our pleasure serving you for your identity. Below are the entered details</p> 
                    <table>
                        <tr><th>Title</th><th>Value</th></tr>';
        $content .= $tableContent;
        $content .= '</table>
                    <p>Please retain this mail as proof till the time you recieve your card.</p>
        
                    <p>We shall process your request soon.</p>
        
                    <p style="margin: 30px 0px 0px;line-height: 24px;">Thank You,</p>
                    <p style="margin: 5px 0px"><a href="http://goldenicard.com/" target="_blank"><img  alt="Logo" title="Logo" style="display:block" width="125" height="87" src="https://golden-lamtouch.s3.ap-south-1.amazonaws.com/GL_logo_black_transaparent.png"></a></p>
                    <p style="margin: 0px; line-height: 24px;">Golden Lamtouch</p>
                    <p style="margin: 0px; line-height: 24px;">We Build Your Identity</p>
        
                    <div style="margin-top: 20px;font-size: 12px;line-height: 18px;">
                        <p style="margin: 0px"><b>Address :</b> Golden Lambtouch, Indraprastha, A/2, Building Flat No.6, Near Sai Chowk, New Sangvi, Pune - 411027.</p>
                        <p style="margin: 0px"><a href="mailTo:www.goldenicard.com">www.goldenicard.com</a> | <a href="mailTo:confirmation@goldenicard.com">confirmation@goldenicard.com</a> | <span>Contact no. (M) : 9422512055, 9422876869</span>
        
                    </div>
                </div>
            </div>
        </body>
        </html>';
        return $content;
    }

    public function getSuccessEmailContentForUser($data, $projectId){

        $arrayKeys = [];
        $arrayTostr =  json_encode($data);
        $strToArray = json_decode($arrayTostr, true);
        $arrayKeys = array_keys($strToArray);
             
        $projectFields = ProjectFields::where('project_id', $projectId)->get()->all();
        $tableContent = '';
        if(count($projectFields) > 0){
            foreach($projectFields as $projectField){
                if (in_array($projectField->project_field_name, $arrayKeys)){
                    if($projectField->project_field_type == "image"){
                        $tableContent .= '<tr><td><p class="head" style="float: none;">'.$projectField->project_field_name.'</p><div class="imgBox" style="width: 150px;height: 150px;box-sizing: border-box;"><img style="width: 150px;height: 150px;" src="'.$strToArray[$projectField->project_field_name].'" alt="'.$projectField->project_field_name.'"></div></td></tr>';
                    }else{
                        $tableContent .= '<tr><td width="100%"><p class="head">'.$projectField->project_field_name.'</p><p>: '.$strToArray[$projectField->project_field_name].'</p></td></tr>';
                    }
                }
            }
        }

        $content =  '';
        $content .=  '<!DOCTYPE html>
        <html>
            <head>
                <title>Golden Lambtouch</title>	
                <style type="text/css">
                    @font-face {
                        src: url("https://golden-lamtouch.s3.ap-south-1.amazonaws.com/ubuntu.regular.ttf");
                        font-family: "ubuntu-regular";
                    }
                    body {
                        font-family: "ubuntu-regular";
                    }
                    table p.head {
                        color: #000;
                        font-weight: 600;
                    }
                    table p {
                        float: left;
                        width: 50%;
                    }
                    table p {
                        font-size: 14px;
                        line-height: 20px;
                        color: #717070;
                        margin: 5px 0px;
                        white-space: nowrap; 
                        overflow: hidden;
                        text-overflow: ellipsis; 
                    }
                    .imgBox {
                        width: 150px;
                        height: 150px;
                        box-sizing: border-box;
                    }
                    .imgBox img {
                        max-width: 100%;
                        max-height: 100%;
                    }
                    
                </style>
            </head>
            <body style="margin: 0px;">
                <div class="mailBody" style="width: 600px; margin: 0px auto 50px;background: #fff;border: 1px solid #488780;" >
                    <div style="background-color: #488780;height: 200px;display: flex;justify-content: center;padding-top: 50px;box-sizing: border-box;">
                        <div class="logo" style="text-align: center;">
                        <a href="http://goldenicard.com/" target="_blank"><img  alt="Logo" title="Logo" style="display:block" width="150px" src="https://golden-lamtouch.s3.ap-south-1.amazonaws.com/GL_logo_black_transaparent.png"></a>
                        </div>
                    </div>
                    <div class="welcomInfo" style="width: 600px;margin: -75px auto 0;background: #fff;position: relative;min-height: 300px;padding: 20px 25px;box-sizing: border-box;">
        
                        <p style="font-size: 16px;color: #717070;line-height: 20px;font-weight: 600;">Dear Applicant,</p>
                        <p style="font-size: 14px;color: #717070;line-height: 20px;">Thank you for sending your application/information to us. It is our pleasure serving you for your identity. Please retain this mail as proof till the time you receive your card.</p>
                        <p style="font-size: 14px;color: #717070;line-height: 20px;">We shall process your request soon.</p>
        
                        <table width="100%">
                            <tbody>';
                            $content .= $tableContent;
                            $content .= '</tbody>
                            </table>
                            
                            <p style="font-size: 14px;line-height: 20px;margin-bottom: 0px;">Thank you</p>
                            <p style="font-size: 14px;line-height: 20px;margin-top: 0px;">The Golden Lamtouch Team</p>
                            <div style="margin-top: 20px;font-size: 12px;line-height: 18px;">
                                <p style="margin: 0px"><b>Address :</b> Golden Lamtouch, Indraprastha, A/2, Building Flat No.6, Near Sai Chowk, New Sangvi, Pune - 411027.</p>
                                <p style="margin: 0px"><a href="mailTo:www.goldenicard.com">www.goldenicard.com</a> | <a href="mailTo:confirmation@goldenicard.com">confirmation@goldenicard.com</a> | <span>Contact no. (M) : 9422512055, 9422876869</span>
                
                            </div>
                        </div>
                        
                    </div>
                </body>
            </html>';
        return $content;
    }
}
    
