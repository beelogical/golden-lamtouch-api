<?php

namespace App\Imports;

use App\TemplateData;
use App\Templates;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class TemplateDataImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new TemplateData([
            'template_name'    => $row["template_name"],
             $data =Templates::where('template_name' ,'Circle')->select('template_id')->get()->first(),
            'organization_id'    => $row["organization_id"],
            'project_id'  => $row["project_id"],
            'user_image'    => $row["user_image"],
            'data'   => $row['data'],
            'template_id'   =>$data->template_id,
            'token_id'   => $row["token_id"],
            'status'    => $row['status'],
            'id_image'   => $row["id_image"],
            'pdf_id'   => $row["pdf_id"],
            'message'   => $row["message"],
        ]);
    }
}
