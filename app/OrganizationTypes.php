<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationTypes extends Model
{
    
    protected $table = 'organization_types';
    protected $primaryKey = 'organization_type_id';
}
