<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrintJobs extends Model
{
    

    protected $table = 'print_jobs';
    protected $primaryKey = 'print_job_id';
    public $timestamps = false;

}
