<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectFieldDropdown extends Model
{
    

    protected $table = 'project_field_dropdown';
    protected $primaryKey = 'dropdown_id';
    public $timestamps = false;
}
