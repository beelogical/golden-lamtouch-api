<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectFields extends Model
{
    protected $table = 'project_fields';
    protected $primaryKey = 'project_field_id';
    public $timestamps = false;
}
