<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateData extends Model
{
    
    protected $table = 'template_data';
    protected $primaryKey = 'template_data_id';
    protected $fillable =['template_data_id','organization_id','project_id','user_image','data','template_id','token_id','status','id_image','pdf_id','message'];
    public $timestamps = false;
}
