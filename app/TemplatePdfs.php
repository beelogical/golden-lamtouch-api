<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplatePdfs extends Model
{
    protected $table = 'template_pdfs';
    protected $primaryKey = 'template_pdf_id';
    public $timestamps = false;
}
