<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
    protected $table = 'templates';
    protected $primaryKey = 'template_id';
    public $timestamps = false;
}
