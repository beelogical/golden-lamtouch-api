<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', [ApiController::class, 'register']);
Route::post('login', [ApiController::class, 'authenticate']);
Route::post('resetPassword', [ApiController::class, 'resetPassword']);
Route::get('roles', [ApiController::class, 'getRoles']);
Route::post('forgetPassword', [ApiController::class, 'forgetPassword']);
Route::get('getFieldsbyToken/{token}', [ApiController::class, 'getProjectFieldsByToken']);
Route::post('templateData/{templateDataId}', [ApiController::class, 'saveTemplateData']);
//Route::post('auth', [ApiController::class, 'getAuthenticatedUser']);
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('logout', [ApiController::class, 'logout']); 
    Route::get('user/{id}', [ApiController::class, 'getUser']);
    Route::post('user/{id}', [ApiController::class, 'addUser']);
    Route::get('organizationType', [ApiController::class, 'getOrganizationTypes']);
    Route::post('changePassword', [ApiController::class, 'changePassword']);
    Route::post('organization/{id}', [ApiController::class, 'addOrganization']);
    Route::get('organization', [ApiController::class, 'getOrganizations']);
    Route::get('organizationProjects/{id}', [ApiController::class, 'getProjects']);
    Route::post('addOrganization', [ApiController::class, 'addOrganization']);
    Route::get('user', [ApiController::class, 'getAllUsers']);
    Route::delete('user/{id}', [ApiController::class, 'deleteUser']);
    Route::delete('organization/{id}', [ApiController::class, 'deleteOrganization']);
    Route::get('fields', [ApiController::class, 'getFields']);
    Route::get('fieldDataTypes', [ApiController::class, 'getFieldDataType']);
    Route::post('project/{id}', [ApiController::class, 'addProject']); 
    Route::get('project/{id}', [ApiController::class, 'getProjectById']); 
    Route::delete('project/{id}', [ApiController::class, 'deleteProject']);
    Route::get('getProjectTokens/{id}', [ApiController::class, 'getProjectTokens']); 
    Route::get('getProjectTokenData/{id}', [ApiController::class, 'getProjectTokenData']); 
    Route::get('getProjectFields/{id}', [ApiController::class, 'getProjectFields']); 
    Route::get('projects', [ApiController::class, 'getAllProjects']); 
    Route::post('templates/{id}', [ApiController::class, 'addTemplates']);
    Route::delete('templates/{id}', [ApiController::class, 'deleteTemplates']);
    Route::get('projectTemplates/{projectId}', [ApiController::class, 'getTemplates']);  
    Route::get('templateData', [ApiController::class, 'getAllTemplateData']); 
    Route::post('importExcel', [ApiController::class, 'importExcel']);
    Route::get('getTemplateDatabyProject/{projectId}', [ApiController::class, 'getTemplateDatabyProject']); 
    Route::get('dashboard', [ApiController::class, 'getDashboardData']);
    
    //Route::get('payment-create/{amount}/{token}', [ApiController::class, 'createPayment']); 
    //Route::post('payment', [ApiController::class, 'payment']); 
    
    Route::post('yearWiseJobs/{year}', [ApiController::class, 'getYearWisePrintJobs']); 
    Route::get('getSubmissions', [ApiController::class, 'getSubmissions']);
    Route::post('saveSubmission', [ApiController::class, 'saveSubmission']); 
    Route::post('saveMultipleSubmissions', [ApiController::class, 'saveMultipleSubmissions']);
    Route::post('saveMultipleTemplateData', [ApiController::class, 'saveMultipleTemplateData']); 
});
Route::post('payment-done', [ApiController::class, 'donePayment']); 
